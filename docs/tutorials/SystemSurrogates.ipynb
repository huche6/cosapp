{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Surrogate models\n",
    "\n",
    "CoSApp allows one to dynamically transform a `System` into a surrogate model.\n",
    "\n",
    "Three class methods of `System` are available to create and manipulate meta-models:\n",
    "\n",
    "* `make_surrogate`\n",
    "* `dump_surrogate`\n",
    "* `load_surrogate`\n",
    "\n",
    "This tutorial presents a walkthrough example to use them.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start by defining a simple module `MultiplyByM` that multiplies an input by a constant factor `m`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System, Port\n",
    "\n",
    "class FloatPort(Port):\n",
    "    \"\"\"Simple port containing a single float variable\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_variable('value', 0.0)\n",
    "\n",
    "class MultiplyByM(System):\n",
    "    \"\"\"System computing y = m * x\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_inward('m')\n",
    "        self.add_input(FloatPort, 'x')\n",
    "        self.add_output(FloatPort, 'y')\n",
    "    \n",
    "    def compute(self):\n",
    "        self.y.value = self.m * self.x.value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now define a head system with two `MultiplyByM`-type children `a` and `b`, which ultimately computes `out.value = a.m * b.m * x.value + 3`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class AffineSystem(System):\n",
    "    \"\"\"System computing out = a.m * b.m * x + 3\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_output(FloatPort, 'out')\n",
    "        # sub-systems\n",
    "        self.add_child(MultiplyByM('a'), pulling='x')\n",
    "        self.add_child(MultiplyByM('b'))\n",
    "        # connections\n",
    "        self.connect(self.a.y, self.b.x)\n",
    "    \n",
    "    def compute(self):\n",
    "        self.out.value = self.b.y.value + 3\n",
    "    \n",
    "    @property\n",
    "    def expected_output(self) -> float:\n",
    "        \"\"\"Expected value for `self.out.value`\"\"\"\n",
    "        return self.a.m * self.b.m * self.x.value + 3\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creation of an `AffineSystem` instance, and sanity check:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = AffineSystem('head')\n",
    "\n",
    "head.x.value = -4.5\n",
    "head.a.m = -1.25\n",
    "head.b.m = 2.52\n",
    "\n",
    "head.run_drivers()\n",
    "\n",
    "assert abs(head.out.value / head.expected_output - 1) < 1e-15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transforming a system into a surrogate model\n",
    "\n",
    "A surrogate model can be regarded as a black box returning a vector of outputs $Y$ from a vector of inputs denoted by $X$, that is $Y = F(X)$.\n",
    "The creation of such model requires a *training* step, whereby function $F$ will be sought from a set of $n$ input vectors $X$, and the corresponding $n$ expected $Y$ vectors.\n",
    "\n",
    "In CoSApp, the transformation of inputs into outputs is performed by systems.\n",
    "Creating a system surrogate model thus requires only a dataset of *input values*, representing a design of experiments (DOE) to be executed.\n",
    "\n",
    "The DOE required by CoSApp must be a dictionary or a `pandas.DataFrame` object whose keys/columns are the names of all input variables representing $X$, and whose values contain the model parameter space.\n",
    "\n",
    "In the case of `AffineSystem`, free inputs are `x`, `a.m` and `b.m`. Therefore, a suitable DOE should contain keys or columns 'x.value', 'a.m' and 'b.m'.\n",
    "\n",
    "DOE datasets should provide meaningful information on the parameter space targetted by the surrogate model.\n",
    "For the sake of example, here, we simply generate a Cartesian grid DOE by sampling variables in individual intervals, and generating all possible compinations, using `itertools.product`.\n",
    "In practice, we recommend the use of more advanced methods to generate DOEs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "import pandas\n",
    "import itertools\n",
    "\n",
    "def Cartesian_DoE(axes: dict) -> pandas.DataFrame:\n",
    "    \"\"\"Simple Cartesian grid DoE from 1D samples in all axis directions\"\"\"\n",
    "    return pandas.DataFrame(\n",
    "        list(itertools.product(*axes.values())),\n",
    "        columns = list(axes.keys()),\n",
    "    )\n",
    "\n",
    "axes = {\n",
    "    'x.value': numpy.linspace(-5, 5, 11),\n",
    "    'a.m': numpy.linspace(-3, 3, 6),\n",
    "    'b.m':numpy.linspace(-3, 3, 6),\n",
    "}\n",
    "\n",
    "doe = Cartesian_DoE(axes)\n",
    "\n",
    "print(doe)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now turn system `head` into a surrogate model, using method `make_surrogate`.\n",
    "Note that the method returns the created surrogate object, of type `SystemSurrogate`; however, you do not need to retrieve it for normal use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head.make_surrogate(doe)\n",
    "\n",
    "print(f\"head.has_surrogate: {head.has_surrogate}\") "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Training is done! Let's set input values and try it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define reference values of inputs\n",
    "# x, a.m and b.m within DoE bounds:\n",
    "ref_state = {\n",
    "    'x.value': -4.5,\n",
    "    'a.m': -1.25,\n",
    "    'b.m': 2.52,\n",
    "}\n",
    "\n",
    "# Set inputs and assert everything works well\n",
    "for var, value in ref_state.items():\n",
    "    head[var] = value\n",
    "\n",
    "head.run_drivers()\n",
    "\n",
    "# Result comparison\n",
    "print(f\"Exact value: {head.expected_output:.15}\")\n",
    "print(\"System computation:\", head.out.value)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** in this example, `head.run_drivers()` is equivalent to a simple `head.run_once()` command, as the system has no cyclic input dependencies. Here, we stick to `run_drivers` for the sake of generality. If you want to know more about meta-models and nonlinear solvers, check out the [advanced tutorial](./SystemSurrogatesAdvanced.ipynb) on surrogate models!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Switch between normal and surrogate system behaviour\n",
    "\n",
    "You can activate and deactivate a surrogate model interactively, using attribute `active_surrogate`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Deactivate surrogate model and revert to normal\n",
    "head.active_surrogate = False\n",
    "assert head.has_surrogate\n",
    "assert not head.active_surrogate\n",
    "head.run_drivers()\n",
    "\n",
    "# Result comparison\n",
    "print(f\"Exact value: {head.expected_output:.15}\")\n",
    "print(\"System computation:\", head.out.value, f\"(Activated surrogate: {head.active_surrogate})\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparison of response surfaces with `x.value` in range \\[-5, 5\\], `a.m` in \\[-3, 3\\], and `b.m` = 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from plotly.subplots import make_subplots\n",
    "import plotly.graph_objects as go\n",
    "\n",
    "# Initialize data\n",
    "head.b.m = 1.\n",
    "plot_axes = {\n",
    "    'a.m': numpy.linspace(-3, 3, 7),\n",
    "    'x.value': numpy.linspace(-5, 5, 11),\n",
    "}\n",
    "dataset = Cartesian_DoE(plot_axes)\n",
    "x, m0 = numpy.meshgrid(plot_axes['x.value'], plot_axes['a.m'])\n",
    "x, m0 = x.flatten(), m0.flatten()\n",
    "\n",
    "####################### DEFINE UTILITY FUNCTIONS FOR PLOTTING #######################\n",
    "\n",
    "def build_data(activate):\n",
    "    head.active_surrogate = activate\n",
    "    z_data = list()\n",
    "    for label, row in dataset.iterrows():\n",
    "        # Set inputs and execute system\n",
    "        for var, value in row.items():\n",
    "            head[var] = value\n",
    "        head.run_drivers()\n",
    "        # Retrieve and store output value\n",
    "        z_data.append(head['out.value'])\n",
    "    return numpy.array(z_data)\n",
    "\n",
    "def add_trace(figure, row, col, ptype, z, **options):\n",
    "    figure.add_trace(\n",
    "        ptype(x=x, y=m0, z=z, **options),\n",
    "        row=row, col=col,\n",
    "    )\n",
    "\n",
    "########################## GETTING COMPUTATION RESULTS ##############################\n",
    "\n",
    "z_normal = build_data(activate=False)\n",
    "z_meta = build_data(activate=True)\n",
    "z_error = numpy.absolute(z_meta - z_normal)\n",
    "\n",
    "################################## MAKE PLOTS #######################################\n",
    "\n",
    "fig = make_subplots(\n",
    "    rows=1, cols=2,\n",
    "    specs=[[{'type': 'surface'}, {'type': 'surface'}]],\n",
    "    subplot_titles=(\n",
    "        \"Response surface\",\n",
    "        f\"Error (max={numpy.linalg.norm(z_error, numpy.inf):.2e})\",\n",
    "    ),\n",
    ")\n",
    "\n",
    "xy_template = 'x.value: %{x:.2f}<br>a.m: %{y:.2f}'\n",
    "\n",
    "# Add normal\n",
    "add_trace(fig, 1, 1, go.Scatter3d, z_normal,\n",
    "    name='Original', mode='markers',\n",
    "    marker=dict(size=4, opacity=1, color='red'),\n",
    "    hovertemplate = xy_template + '<br>out.value: %{z:.2f}',\n",
    ")\n",
    "\n",
    "# Add meta\n",
    "add_trace(fig, 1, 1, go.Mesh3d, z_meta,\n",
    "    name=\"Meta-model\", opacity=0.5, color='blue',\n",
    "    hovertemplate = xy_template + '<br>out.value: %{z:.2f}',\n",
    ")\n",
    "\n",
    "# Add error\n",
    "add_trace(fig, 1, 2, go.Mesh3d, z_error,\n",
    "    name=\"Error\", opacity=0.5,\n",
    "    hovertemplate = xy_template + '<br>error: %{z:.2e}',\n",
    ")\n",
    "\n",
    "fig.update_traces(showlegend=True)\n",
    "fig.update_layout(\n",
    "    title = dict(text=f\"Behaviour of system {head.name!r}\", y=0.95, x=0.5, xanchor='center', yanchor='top'),\n",
    "    scene1 = dict(\n",
    "        xaxis_title='x.value',\n",
    "        yaxis_title='a.m',\n",
    "        zaxis_title='out.value',\n",
    "    ),\n",
    "    scene2 = dict(\n",
    "        xaxis_title='x.value',\n",
    "        yaxis_title='a.m',\n",
    "        zaxis=dict(title=\"Error\", type='log'),\n",
    "    ),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Saving and reusing a meta-model\n",
    "\n",
    "Training a surrogate model is usually a computationally expensive operation.\n",
    "Once a surrogate model has been created, you may save it into a (binary) file using method `dump_surrogate`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head.dump_surrogate('AffineSystemMeta.pickle')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dumped model can be loaded from the `.pickle` file so we don't have to train it again, using method `load_surrogate`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(System.load_surrogate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example below, we use read-only attribute `has_surrogate` to assert whether or not the system has been meta-modeled."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = AffineSystem('head')  # Create new model from scratch\n",
    "assert not head.has_surrogate\n",
    "\n",
    "head.load_surrogate('AffineSystemMeta.pickle')\n",
    "assert head.has_surrogate\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "System `head` now possesses a usable surrogate model, without prior training:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set input values defined previously\n",
    "for var, value in ref_state.items():\n",
    "    head[var] = value\n",
    "\n",
    "head.run_once()\n",
    "print(f\"Active surrogate model: {head.active_surrogate}\")\n",
    "print(f\"Output of surrogate model is {head.out.value:.5f} instead of {head.expected_output:.15}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** a surrogate model file can be loaded by several systems.\n",
    "If you have a four-engine plane model, train only one engine sub-system, and load the other three from a saved `.pickle` file!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "other = AffineSystem('other')\n",
    "other.load_surrogate('AffineSystemMeta.pickle')\n",
    "\n",
    "for var, value in ref_state.items():\n",
    "    other[var] = value\n",
    "\n",
    "other.run_drivers()\n",
    "print(f\"Output of {other.name!r} is {other.out.value:.5f} instead of {other.expected_output:.15}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "os.remove('AffineSystemMeta.pickle')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use your own surrogate models\n",
    "\n",
    "Module `cosapp.utils.surrogate_models` contains models readily usable in `make_surrogate`, with optional argument `model`:\n",
    "```python\n",
    "from cosapp.utils.surrogate_models import FloatKrigingSurrogate\n",
    "\n",
    "system.make_surrogate(doe, model=FloatKrigingSurrogate)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cosapp.utils.surrogate_models as surrogate_models\n",
    "import inspect\n",
    "\n",
    "def get_class_names(module):\n",
    "    \"\"\"Returns the names of all classes found in `module`.\"\"\"\n",
    "    return [name for (name, cls) in inspect.getmembers(module, inspect.isclass)]\n",
    "\n",
    "get_class_names(surrogate_models)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Additionally, you may define and use your own surrogate models.\n",
    "Valid models must be concrete implementations of interface `cosapp.base.SurrogateModel`, containing two abstract methods `train` and `predict`.\n",
    "\n",
    "For instance, encapsulating an algorithm from scikit-learn may look like:\n",
    "\n",
    "```python\n",
    "from cosapp.base import SurrogateModel\n",
    "from sklearn import svm\n",
    "\n",
    "class SupportVectorRegression(SurrogateModel):\n",
    "    def __init__(self, kernel=\"rbf\", **options):\n",
    "        self.__regr = svm.SVR(kernel=kernel, **options)\n",
    "\n",
    "    def train(self, x, y):\n",
    "        self.__regr.fit(x, y)\n",
    "    \n",
    "    def predict(self, x):\n",
    "        return self.__regr.predict(x)\n",
    "\n",
    "# Use your model - additional keyword arguments such as `kernel` are forwarded to the model\n",
    "system.make_surrogate(doe, model=SupportVectorRegression, kernel=\"poly\")\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.7.8 64-bit ('cosappdev': conda)",
   "metadata": {
    "interpreter": {
     "hash": "9eae103dcdd30fbd3b97f875860b4cabfb419e4b56fe365454fd2c0b381adddb"
    }
   },
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.8-final"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
