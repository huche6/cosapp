{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![CoSAppLogo](images/cosapp.svg)\n",
    "\n",
    "# Custom connectors\n",
    "\n",
    "Connectors ensure data transfer from a *source* port to a *sink* port.\n",
    "By default, they are of type `cosapp.ports.Connector`, which transmits shallow copies of source variables to sink variables, and performs unit conversion when necessary.\n",
    "\n",
    "It is also possible for users to define their own connector classes, as long as they derive from base class `cosapp.base.BaseConnector`, and implement abstract method `transfer`.\n",
    "Two cases are possible:\n",
    "\n",
    "1. Specific port connectors, for peer-to-peer connections.\n",
    "\n",
    "2. Generic connectors, capable of handling any source/sink pair, including ports of different types.\n",
    "\n",
    "This tutorial briefly illustrates these two situations.\n",
    "\n",
    "**Warning:** data transfer is a fundamental feature of multi-system simulations, so we strongly discourage the use of custom general connectors, unless strictly necessary.\n",
    "In particular, custom connectors should never be used as a substitute for an additional system in a composite model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Peer-to-peer connectors\n",
    "\n",
    "Specific port connectors are implemented as an inner class `Connector`, defined inside the targetted port class.\n",
    "When a specific port connector class exists, all peer-to-peer connections (including those created by a *pulling*) will automatically use it.\n",
    "\n",
    "In the example below, we define port class `XyzPort`, with peer-to-peer connector `XyzPort.Connector`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import Port, System, BaseConnector\n",
    "from cosapp.utils import set_log, LogLevel\n",
    "import numpy\n",
    "\n",
    "\n",
    "class XyzPort(Port):\n",
    "    \"\"\"Port with a specific peer-to-peer connector\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_variable('x', 1.0)\n",
    "        self.add_variable('y', 2.0)\n",
    "        self.add_variable('z', 3.0)\n",
    "\n",
    "    class Connector(BaseConnector):\n",
    "        \"\"\"Custom connector for `XyzPort` objects\n",
    "        \"\"\"\n",
    "        def __init__(self, name: str, sink: Port, source: Port, *args, **kwargs):\n",
    "            super().__init__(name, sink, source, mapping=dict(zip('xyz', 'xzy')))\n",
    "        \n",
    "        def transfer(self) -> None:\n",
    "            source = self.source\n",
    "            self.sink.set_values(\n",
    "                x = source.x,\n",
    "                y = source.z,\n",
    "                z = -source.y,\n",
    "            )\n",
    "\n",
    "\n",
    "class XyzSystem(System):\n",
    "    def setup(self):\n",
    "        self.add_input(XyzPort, 'p_in')\n",
    "        self.add_output(XyzPort, 'p_out')\n",
    "\n",
    "    def compute(self):\n",
    "        p_in = self.p_in\n",
    "        self.p_out.set_values(\n",
    "            x = p_in.x * 2,\n",
    "            y = p_in.y - p_in.x,\n",
    "            z = p_in.z - p_in.x,\n",
    "        )\n",
    "\n",
    "\n",
    "class XyzToVector(System):\n",
    "    def setup(self):\n",
    "        self.add_input(XyzPort, 'p_in')\n",
    "        self.add_outward('v', numpy.zeros(3))\n",
    "\n",
    "    def compute(self):\n",
    "        p = self.p_in\n",
    "        self.v = numpy.array([p.x, p.y, p.z])\n",
    "\n",
    "\n",
    "top = System('top')\n",
    "s1 = top.add_child(XyzSystem('s1'))\n",
    "s2 = top.add_child(XyzToVector('s2'))\n",
    "\n",
    "set_log()\n",
    "top.connect(s1.p_out, s2.p_in)  # automatically uses `XyzPort.Connector`\n",
    "\n",
    "s1.p_in.set_values(x=0.1, y=1.0, z=2.0)\n",
    "top.run_once()\n",
    "\n",
    "print()\n",
    "for var in (\"s1.p_in\", \"s1.p_out\", \"s2.p_in\", \"s2.v\"):\n",
    "    print(f\"top.{var}:\\t{top[var]!r}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General connectors\n",
    "\n",
    "General connectors are supposed to handle data transfer between any source/sink port pair.\n",
    "We show below two examples of such connectors;\n",
    "\n",
    "- `PlainConnector`, transfering data using a simple assignment syntax;\n",
    "- `DeepCopyConnector`, assigning deep copies of source variables to sink variables. More information on shallow and deep copy may be found [here](https://docs.python.org/3/library/copy.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import BaseConnector\n",
    "import copy\n",
    "\n",
    "\n",
    "class PlainConnector(BaseConnector):\n",
    "    \"\"\"Plain connector performing simple variable assignments.\n",
    "    See `BaseConnector` for base class details.\n",
    "    \"\"\"\n",
    "    def transfer(self) -> None:\n",
    "        source, sink = self.source, self.sink\n",
    "\n",
    "        for target, origin in self.mapping.items():\n",
    "            # Implement: sink.target = source.origin\n",
    "            setattr(sink, target, getattr(source, origin))\n",
    "\n",
    "\n",
    "class DeepCopyConnector(BaseConnector):\n",
    "    \"\"\"Deep copy connector.\n",
    "    \"\"\"\n",
    "    def transfer(self) -> None:\n",
    "        source, sink = self.source, self.sink\n",
    "\n",
    "        for target, origin in self.mapping.items():\n",
    "            # Implement: sink.target = deepcopy(source.origin)\n",
    "            value = getattr(source, origin)\n",
    "            setattr(sink, target, copy.deepcopy(value))\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For most data structures, plain assignment in Python usually binds the two sides of the equal sign by simple reference, with no copy.\n",
    "This, in fact, is the reason why the default connector class `cosapp.ports.Connector` perform copies, to avoid undesired side effects, such as trivially nil residues during loop resolution, for instance. \n",
    "\n",
    "In the next example, we use `PlainConnector` in a port connection involving `numpy` arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import Port, System\n",
    "import numpy\n",
    "\n",
    "\n",
    "class FramePort(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable('x', numpy.zeros(3), unit='m', desc=\"Centre-of-mass position\")\n",
    "        self.add_variable('w', numpy.zeros(3), unit='1/s', desc=\"Angular velocity\")\n",
    "\n",
    "\n",
    "class Displacement(System):\n",
    "    def setup(self):\n",
    "        self.add_input(FramePort, 'fr_in')\n",
    "        self.add_output(FramePort, 'fr_out')\n",
    "        self.add_inward('delta', numpy.zeros(3), unit='m', desc=\"Displacement vector\")\n",
    "\n",
    "    def compute(self):\n",
    "        self.fr_out.x = self.fr_in.x + self.delta\n",
    "        self.fr_out.w = self.fr_in.w\n",
    "\n",
    "\n",
    "top = System('top')\n",
    "d1 = top.add_child(Displacement('d1'), pulling='fr_in')\n",
    "d2 = top.add_child(Displacement('d2'), pulling='fr_out')\n",
    "\n",
    "top.connect(d1.fr_out, d2.fr_in, cls=PlainConnector)  # specify connector\n",
    "\n",
    "top.fr_in.x = numpy.zeros(3)\n",
    "top.d1.delta = numpy.r_[0.0, 0.1, 0.2]\n",
    "top.d2.delta = numpy.r_[1.0, 0.5, 0.0]\n",
    "\n",
    "# Before system execution, connected arrays are different objects:\n",
    "assert d1.fr_out.x is not d2.fr_in.x\n",
    "\n",
    "# After system execution, arrays are identical objects,\n",
    "# owing to simple assignment in `PlainConnector`:\n",
    "top.run_once()\n",
    "\n",
    "print(\n",
    "    f\"d1.fr_out.x is d2.fr_in.x: {d1.fr_out.x is d2.fr_in.x}\",\n",
    "    f\"top.fr_out.x = {top.fr_out.x}\",\n",
    "    sep=\"\\n\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try it out with `DeepCopyConnector` (or nothing) instead of `PlainConnector`, and see what happens!"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notes\n",
    "\n",
    "- **This stunt was performed by trained professionals, do not try this at home!**\n",
    "In this very simple example, the absence of copy may lead to a slight improvement in performance, but can have very bad, hard-to-debug side effects.\n",
    "- `DeepCopyConnector` is safe, and readily available in module `cosapp.ports.connectors`. It may be useful when one wants to ensure a complete, recursive copy of complex composite data structures, but is slower than default class `Connector`. Importantly, it does *not* perform unit conversion.\n",
    "- Module `cosapp.ports.connectors` also contains a shallow copy connector, named `CopyConnector`. Unlike `Connector`, though, it does not handle unit conversion.\n",
    "- If a port contains a type-specific connector, peer-to-peer connections use the dedicated connector by default (see [Peer-to-peer connectors](#Peer-to-peer-connectors)). However, this behaviour can be overriden by specifying optional argument `cls` in `System.connect`."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Direct value connectors `PlainConnector`, `CopyConnector` and `DeepCopyConnector` (from module `cosapp.ports.connectors`) can be useful to define peer-to-peer connectors.\n",
    "For instance:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import Port\n",
    "from cosapp.ports.connectors import CopyConnector\n",
    "import numpy\n",
    "\n",
    "\n",
    "class FramePort(Port):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_variable('x', numpy.zeros(3), unit='m', desc=\"Centre-of-mass position\")\n",
    "        self.add_variable('w', numpy.zeros(3), unit='1/s', desc=\"Angular velocity\")\n",
    "\n",
    "    Connector = CopyConnector  # port-specific peer-to-peer connector"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "cosapp",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "name": "python",
   "version": "3.9.13 | packaged by conda-forge | (main, May 27 2022, 16:50:36) [MSC v.1929 64 bit (AMD64)]"
  },
  "vscode": {
   "interpreter": {
    "hash": "03d8647662c9fbe9220ebb6c4a5dd3c1d557fb5efab079901b8383e5f052f0cc"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
