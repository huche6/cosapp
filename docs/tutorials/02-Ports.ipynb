{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ports\n",
    "\n",
    "## What is a `Port`?!\n",
    "\n",
    "A `Port` is a collection of variables that helps transfer data from one `System` to another.\n",
    "Without such an object, connections between systems should be done variable-by-variable, *outch!*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Connected ports](images/ports_1.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Types\n",
    "\n",
    "### `IN` or `OUT`\n",
    "\n",
    "Port instances inside systems are directional objects; they can be either `IN` or `OUT`. This definition ensures compatibility at connection.\n",
    "However, the definition of a `Port` class only contains the list and description of its variables, irrespective of direction.\n",
    "\n",
    "### Available ports and compatibility\n",
    "\n",
    "<font color='orange'>**CoSApp**</font> allow user-defined ports, stored in libraries or created in specific projects. They define a frozen collection of variables that can be shared between multiple systems. They are compatible by construction (see the [Port connection](#Port-connection) section of this tutorial).\n",
    "\n",
    "## Create a port\n",
    "\n",
    "### Import CoSApp core package"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import CoSApp base classes\n",
    "from cosapp.base import System, Port"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Define a new port"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class DemoPort(Port):\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_variable('a', 1.0)\n",
    "        self.add_variable('b', 2.0)\n",
    "        self.add_variable('c', 3.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Use it in a system\n",
    "\n",
    "We now create a new `System` using `DemoPort`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class DemoSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(DemoPort, 'p_in')\n",
    "        self.add_output(DemoPort, 'p_out') \n",
    "\n",
    "    def compute(self):\n",
    "        self.p_out.a = self.p_in.c\n",
    "        self.p_out.b = self.p_in.b\n",
    "        self.p_out.c = self.p_in.a\n",
    "\n",
    "s = DemoSystem(name='s')\n",
    "# Set `s.p_in` variables one by one...\n",
    "s.p_in.a = 0.2\n",
    "s.p_in.b = 0.5\n",
    "# ... or use multi-variable setter `set_values`:\n",
    "s.p_in.set_values(a=0.2, c=1.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example above, `p_in` and `p_out` are two instances of `DemoPort`.\n",
    "All instances of `DemoSystem`, such as `s`, will possess attributes `p_in` and `p_out`.\n",
    "Use of methods `add_input` and `add_output` provides a clear interface to create ports with the desired direction.\n",
    "\n",
    "![Port-DemoSystem](images/ports_2.svg)\n",
    "\n",
    "Run the system to confirm the expected behaviour"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.run_once()\n",
    "s.p_in"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.p_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set information on the variables\n",
    "\n",
    "All `Port` variables may be given optional information:\n",
    "\n",
    "- `unit`: Physical unit of the variable, given by a string. Units *are not enforced* inside a `System`.\n",
    "This means that system developers are responsible for unit conversions (if required) in method `compute`. However, **CoSApp** will take care of unit conversion during data transfer via a connector (see [Port connection](#Port-connection)).\n",
    "- `desc`: Short description of the variable.\n",
    "- `dtype`: If you need to force certain data type(s) on a variable, a tuple of acceptable types can be provided\n",
    "through this keyword. If that information is not supplied, dtype is inferred from the variable value; e.g.\n",
    "a number (integer or floating point) will be typed as `Number`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class AdvancedDemoPort(Port):\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_variable('a', 3e2, unit='degK', dtype=float, desc='Temperature')\n",
    "        self.add_variable('b', 0.1, unit='MPa', dtype=(int, float), desc='Pressure')\n",
    "        self.add_variable('c', 1.0, unit='kg/s', desc='Mass flowrate')\n",
    "\n",
    "\n",
    "class AdvancedDemoSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(AdvancedDemoPort, 'p_in')\n",
    "        self.add_output(AdvancedDemoPort, 'p_out') \n",
    "\n",
    "    def compute(self):\n",
    "        self.p_out.a = self.p_in.c\n",
    "        self.p_out.b = self.p_in.b\n",
    "        self.p_out.c = self.p_in.a\n",
    "\n",
    "\n",
    "sa = AdvancedDemoSystem('sa')\n",
    "print('Input port:')\n",
    "sa.p_in"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Port connection\n",
    "\n",
    "### Introduction\n",
    "\n",
    "Port connection creates a dedicated object (of type `Connector`) managing one-way data transfers between two `Port` instances.\n",
    "Connectors are handled at system levels, via method `connect`, and can be accessed through attribute `connectors` as a dictionnary.\n",
    "\n",
    "![Port-Connection](images/ports_3.svg)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h = System('head')\n",
    "h.add_child(DemoSystem('demo1'))\n",
    "h.add_child(DemoSystem('demo2'))\n",
    "\n",
    "h.connect(h.demo2.p_in, h.demo1.p_out)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you need to connect a subset of the `Port` variables, or connect inwards or outwards, the `connect()` method of `System` can be called in `setup` with additional arguments, providing variable mapping from the first port to the other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MonitorSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_inward('a')\n",
    "        self.add_inward('b')\n",
    "        self.add_inward('x')\n",
    "        self.add_outward('result')\n",
    "\n",
    "    def compute(self):\n",
    "        self.result = self.a\n",
    "\n",
    "h.add_child(MonitorSystem('monitor'))\n",
    "# Connect `h.monitor.a` to `h.demo1.p_out.a`\n",
    "h.connect(h.monitor.inwards, h.demo1.p_out, 'a')\n",
    "\n",
    "h.demo1.p_in.set_values(a=50., b=0., c=25.)\n",
    "h.run_once()\n",
    "\n",
    "print(f\"h.monitor.a = {h.monitor.a}\")\n",
    "h.demo1.p_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example above, variable `a` from port `inwards` has been connected to variable `a` from port `demo1.p_out`.\n",
    "This is the simplest mapping, when the variable name is identical in poth ports.\n",
    "\n",
    "Other options are possible:\n",
    "\n",
    "- A list variable names, if all names exist in both ports:\n",
    "```python\n",
    "    h.connect(h.monitor.inwards, h.demo1.p_out, ['a', 'b'])\n",
    "```\n",
    "\n",
    "- An explicit name mapping, if names differ:\n",
    "```python\n",
    "    h.connect(h.monitor.inwards, h.demo1.p_out, {'a': 'a', 'x': 'c'})\n",
    "```\n",
    "\n",
    "- Connecting two systems, with a port and/or variable mapping:\n",
    "```python\n",
    "    h.connect(h.monitor, h.demo1, {'x': 'p_out.c'})\n",
    "```\n",
    "In the last example, `h.x` will be connected to `h.demo1.p_out.c`.\n",
    "Note that `h.x` and `h.inwards.x` refer to the same variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h.demo2.p_in"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h.monitor.outwards"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example with unit conversions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class SiFlowPort(Port):\n",
    "    \"\"\"Flow port in SI units\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_variable('q', 1.0, unit='kg/s', desc='Mass flowrate')\n",
    "        self.add_variable('p', 1e5, unit='Pa', dtype=(int, float), desc='Pressure')\n",
    "        self.add_variable('T', 3e2, unit='degK', dtype=float, desc='Temperature')\n",
    "\n",
    "\n",
    "class UsFlowPort(Port):\n",
    "    \"\"\"Flow port in US units\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_variable('q', 1.0, unit='lb/min', desc='Mass flowrate')\n",
    "        self.add_variable('p', 14.5, unit='psi', dtype=(int, float), desc='Pressure')\n",
    "        self.add_variable('T', 32.0, unit='degF', dtype=float, desc='Temperature')\n",
    "\n",
    "\n",
    "class SiUnitSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(SiFlowPort, 'fl_in')\n",
    "        self.add_output(SiFlowPort, 'fl_out') \n",
    "\n",
    "    def compute(self):\n",
    "        # Simply assign `in` values to `out` port\n",
    "        self.fl_out.set_from(self.fl_in)\n",
    "\n",
    "\n",
    "class UsUnitSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(UsFlowPort, 'fl_in')\n",
    "        self.add_output(UsFlowPort, 'fl_out') \n",
    "\n",
    "    def compute(self):\n",
    "        # Simply assign `in` values to `out` port\n",
    "        self.fl_out.set_from(self.fl_in)\n",
    "\n",
    "\n",
    "class TemperatureCheck(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_inward('T', 0.0, unit='degC')\n",
    "        self.add_inward('Tmax', 60.0, unit='degC')\n",
    "        self.add_outward('too_hot', False) \n",
    "\n",
    "    def compute(self):\n",
    "        self.too_hot = (self.T > self.Tmax)\n",
    "\n",
    "\n",
    "top = System('top')\n",
    "si = top.add_child(SiUnitSystem('si'))\n",
    "us = top.add_child(UsUnitSystem('us'))\n",
    "checker = top.add_child(TemperatureCheck('checker'))\n",
    "\n",
    "top.connect(si.fl_out, us.fl_in)\n",
    "top.connect(us, checker, {'fl_out.T': 'T'})\n",
    "\n",
    "# Set entry conditions\n",
    "top.si.fl_in.set_values(q=2.5, p=1e5, T=293.15)\n",
    "top.run_once()\n",
    "\n",
    "top.si.fl_out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "top.us.fl_in"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"top.checker.too_hot = {top.checker.too_hot}\")\n",
    "\n",
    "top.checker.inwards"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unit compatibility is checked at connection:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "\n",
    "try:\n",
    "    top.connect(us, checker, {'fl_out.p': 'Tmax'})\n",
    "\n",
    "except Exception as error:\n",
    "    logging.error(error)\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Useful functions\n",
    "\n",
    "#### Method `set_values`\n",
    "\n",
    "Method `set_values` allows one to set multiple variables of a port at once, using keyword arguments:\n",
    "```python\n",
    "port.set_values(x=1.2, y=0.5, v=numpy.zeros(3))\n",
    "\n",
    "# Rather than\n",
    "port.x = 1.2\n",
    "port.y = 0.5\n",
    "port.v = numpy.zeros(3)\n",
    "```\n",
    "\n",
    "#### Method `set_from`\n",
    "\n",
    "Method `set_from` assigns port values from another port.\n",
    "If the source and destination ports are of different types, it selects common variable names between them.\n",
    "This behaviour can be overriden by specifying optional argument `check_names=False`.\n",
    "\n",
    "By default, values are simply assigned.\n",
    "If another operation is required, one can specify a transfer function applied before the assignments, via optional argument `transfer`.\n",
    "For example, a shallow copy can be enforced with:\n",
    "```python\n",
    "import copy\n",
    "\n",
    "# Copy variables of `source` port into `destination` port\n",
    "destination.set_from(source, transfer=copy.copy)\n",
    "```\n",
    "\n",
    "#### Iterator `items()`\n",
    "\n",
    "Method `items()` returns an iterator yieding (varname, value) tuples, akin to `dict.items()`:\n",
    "```python\n",
    "for varname, value in port.items():\n",
    "    print(f\"{varname} = {value}\")\n",
    "\n",
    "# Send port variables into a dictionary:\n",
    "data = dict(port.items())\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Congrats!** Now you know the basics of `Port` in **CoSApp**.\n",
    "\n",
    "Next you will discover how to solve mathematical problems using [Drivers](03-Drivers.ipynb)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0"
  },
  "vscode": {
   "interpreter": {
    "hash": "392997bb25b61debd89a48ea61540852af93803f41442b557b3f76ce168a37e6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
