{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Drivers\n",
    "\n",
    "## What is a `Driver`?!\n",
    "\n",
    "`Driver` objects allow users to modify the state of a `System` for a given purpose. Resolution of non-linear equations, optimization, or time integration of dynamic equations are typical examples of `Driver` usage.\n",
    "As will be seen in this tutorial, drivers can also be nested and/or chained, to create a customized simulation workflow.\n",
    "\n",
    "## Introduction\n",
    "\n",
    "### Add a `Driver`\n",
    "\n",
    "Simply use the `add_driver` method passing the `Driver` object you want to use \n",
    "(see [Available drivers](#Available-Drivers) section of this tutorial)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "from cosapp.tests.library.ports import XPort\n",
    "\n",
    "class MultiplySystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(XPort, 'p_in', {'x': 1.})\n",
    "        self.add_output(XPort, 'p_out', {'x': 1.})\n",
    "\n",
    "        self.add_inward('K1', 5.)\n",
    "        \n",
    "    def compute(self):\n",
    "        self.p_out.x = self.p_in.x * self.K1\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import RunOnce\n",
    "\n",
    "m = MultiplySystem('mult')\n",
    "run = m.add_driver(RunOnce('run'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " ![Driver in system](images/drivers_1.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implementation\n",
    "\n",
    "Every `System` (including sub-systems) may have one or multiple `Driver` objects. They are stored in the `drivers` attribute.\n",
    "By default, no `Driver` is attached to a `System`.\n",
    "\n",
    "The `run_drivers` method of `System` executes drivers recursively. In the following example, the simplest driver\n",
    "[RunOnce](#RunOnce) is added and executed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = MultiplySystem('mult')\n",
    "m.add_driver(RunOnce('run'))\n",
    "print('m.drivers:', m.drivers) # print drivers of the system\n",
    "\n",
    "m.K1 = 2.\n",
    "m.p_in.x = 15.\n",
    "m.run_drivers()\n",
    "\n",
    "print(f\"m.p_out.x = {m.p_out.x}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Driver chains and subdrivers\n",
    "\n",
    "If several drivers are attached to a system, `run_drivers` will execute them in turn, as a sequence of drivers.\n",
    "Furthermore, like a `System`, each individual `Driver` may have children, which also inherit from base class `Driver`. Nested drivers are created with method `add_child` of class `Driver`; they are stored in attribute `children` of the parent driver.\n",
    "\n",
    "By construction, a `System` can have as many levels of drivers as required.\n",
    "\n",
    "Driver chains and nested drivers thus allow users to define complex simulation scenarios, such as workflows, multi-point design, designs of experiment, optimization, *etc.*\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![drivers](images/drivers_2.svg)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = MultiplySystem('mult')\n",
    "run = m.add_driver(RunOnce('run'))\n",
    "\n",
    "print(f\"run.children: {run.children}\")  # driver 'run' has no child"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subrun = run.add_child(RunOnce('subrun'))  # add a sub-driver 'subrun'\n",
    "\n",
    "print(f\"run.children: {run.children}\", f\"subrun.children: {subrun.children}\", sep=\"\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Available Drivers\n",
    "\n",
    "**CoSApp** comes with a set of drivers to help users build their simulations.\n",
    "\n",
    "### RunOnce\n",
    "\n",
    "As the name suggests, `RunOnce` makes your `System` and its subsystems compute their code once. It does not deal with residues or iterative loops that may be necessary to resolve the `System`. Instead, it merely transports information from the top system down to the lowest level sub-systems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "from cosapp.tests.library.ports import XPort\n",
    "\n",
    "class MultiplySystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(XPort, 'p_in', {'x': 1.})\n",
    "        self.add_output(XPort, 'p_out', {'x': 1.})\n",
    "\n",
    "        self.add_inward('K1', 5.)\n",
    "        \n",
    "    def compute(self):\n",
    "        self.p_out.x = self.p_in.x * self.K1\n",
    "\n",
    "\n",
    "class MultiplyWithResidue(MultiplySystem):\n",
    "    \"\"\"Same as `MultiplySystem`, including an off-design problem\"\"\"\n",
    "    def setup(self):\n",
    "        super().setup()\n",
    "        \n",
    "        # off-design problem\n",
    "        self.add_inward('expected_output', 7.5)\n",
    "        self.add_unknown('p_in.x').add_equation('p_out.x == expected_output')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import RunOnce\n",
    "\n",
    "m = MultiplyWithResidue('mult')\n",
    "run = m.add_driver(RunOnce('run'))\n",
    "\n",
    "m.run_drivers()\n",
    "\n",
    "print(\"List of defined drivers\\n\", dict(m.drivers))\n",
    "\n",
    "print(\"\",\n",
    "    f\"K1 = {m.K1}\",\n",
    "    f\"p_in.x = {m.p_in.x}\",\n",
    "    f\"p_out.x = {m.p_out.x}\",\n",
    "    f\"residues: {list(m.residues.values())}\",\n",
    "    sep=\"\\n\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### NonLinearSolver\n",
    "\n",
    "This `Driver` determines the parameters of your `System` declared as unknowns that satisfy its equations. It resolves the mathematical problem between free parameters and residues of its child drivers.\n",
    "\n",
    "A `NonLinearSolver` driver also resolves cyclic dependencies of its owner system (if any).\n",
    "\n",
    "Available options are:\n",
    "\n",
    "- *method* - resolution method, to be chosen from enum `cosapp.drivers.NonLinearMethods` (see below for detail). Default is `NonLinearMethods.NR`.\n",
    "- *tol* - solver iterates until *residue <= tol* (for default method, default tol is 'auto', in which case the actual tolerance is computed from estimated numerical noise level).\n",
    "- *factor* - relaxation factor applied at each iteration (1 by default; must be striclty positive).\n",
    "- *max_iter* - maximum number of iterations (500 by default).\n",
    "\n",
    "We define below a new `System` containing an equation, and solve it with a `NonLinearSolver` driver.\n",
    "\n",
    "An `unknown` is simply defined by its name. An `equation` is defined by a string of the kind `\"lhs == rhs\"`, where `lhs` and `rhs` denote the left- and right-hand sides. Each side of the `equation` may be a constant or an evaluable expression, as in `\"x - cos(sub.y) == z**2\"`, or  `\"x == 1.5\"`. An exception will be raised if both sides are trivially constant, as in `\"pi / 2 == 0\"`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "m = MultiplySystem('mult')\n",
    "\n",
    "solver = m.add_driver(NonLinearSolver('solver', max_iter=100, tol=1e-6, factor=0.8))\n",
    "solver.add_unknown('p_in.x').add_equation('p_out.x == 7.5')\n",
    "\n",
    "m.run_drivers()\n",
    "\n",
    "print(\"m.drivers:\", m.drivers)\n",
    "print(m.drivers['solver'].children)\n",
    "print(\"\",\n",
    "    f\"K1 = {m.K1}\",\n",
    "    f\"p_in.x = {m.p_in.x}\",\n",
    "    f\"p_out.x = {m.p_out.x}\",\n",
    "    f\"residues: {list(solver.problem.residues.values())}\",\n",
    "    sep=\"\\n\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Resolution method\n",
    "\n",
    "Several resolution methods are available, through option `method`, of type `NonLinearMethods`, contained in module `cosapp.drivers`.\n",
    "Possible choices are:\n",
    "\n",
    "- `NonLinearMethods.NR` (default, recommended): custom implementation of Newton-Raphson algorithm, tailored for CoSApp systems.\n",
    "- `NonLinearMethods.POWELL`: Powell hybrid method (encapsulation of `scipy.optimize.root`).\n",
    "- `NonLinearMethods.BROYDEN_GOOD`: Broyden's \"good\" method (encapsulation of `scipy.optimize.root`).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### RunSingleCase\n",
    "\n",
    "`NonLinearSolver` is primarily designed to solve [multi-point problems](./08-Multipoints-Design.ipynb), where several [RunSingleCase](#RunSingleCase) drivers are declared as direct sub-drivers of the solver.\n",
    "By default, a `NonLinearSolver` driver comes with one [RunSingleCase](#RunSingleCase) child, called *runner*. \n",
    "\n",
    "![scipysolver](images/drivers_nonlinear.svg)\n",
    "\n",
    "`RunSingleCase` executes all subsystem drivers by recursively calling the `compute()` method of the top system and each of its children.\n",
    "\n",
    "This `Driver` does not contain a solver per se, but is helpfull to set boundary conditions, initial values, and define additional unknowns and/or equations.\n",
    "It is primarily meant to be used as an operating point of a [NonLinearSolver](#NonLinearSolver) driver.\n",
    "Therefore, `RunSingleCase` drivers are usually created as sub-drivers of `NonLinearSolver`, and are seldom directly attached to a system.\n",
    "\n",
    "The state of the owner `System` can be changed with two methods:\n",
    "\n",
    "- `set_values` will impose the value of prescribed input variables, as boundary conditions;\n",
    "- `set_init` will change the initial value of iteratives before resolving the case.\n",
    "\n",
    "Both methods take as argument a dictionary of the kind `{varname: value, ...}`, where `varname` is the name of an input variable *in the context of the driver owner*.\n",
    "For example, if the driver is attached to a system `head` possessing a child named `sub` and an inward `x`, `{'sub.k': 0.0, 'x': 0.1}` will affect variables `head.sub.k` and `head.x`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import RunSingleCase\n",
    "\n",
    "m = MultiplyWithResidue('mult')\n",
    "update = m.add_driver(RunSingleCase('update'))\n",
    "\n",
    "update.set_values({'expected_output': 15, 'K1': 2})\n",
    "update.set_init({'p_in.x': 1.5})\n",
    "m.run_drivers()\n",
    "\n",
    "print(\"List of defined drivers:\", m.drivers, sep=\"\\n\")\n",
    "print(\"\",\n",
    "    f\"K1 = {m.K1}\",\n",
    "    f\"p_in.x = {m.p_in.x}\",\n",
    "    f\"p_out.x = {m.p_out.x}\",\n",
    "    f\"residues: {list(m.residues.values())}\",\n",
    "    sep=\"\\n\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another important ability of this driver is the addition of unknowns and equations to the mathematical system.\n",
    "There are two kinds of problems for a `RunSingleCase` driver:\n",
    "\n",
    "- **Design problems**: They are associated with design variables that are frozen in the final product; e.g. the diameter of a pipe. Design unknowns are uniquely defined, and shared between all design points, when several `RunSingleCase` drivers are present (see tutorial on [multi-point design](08-Multipoints-Design.ipynb)).\n",
    "- **Local off-design problems**: They correspond to constraints imposed at the design point only. Local `RunSingleCase` unknowns will usually assume different values at different design points.\n",
    "\n",
    "Design and off-design problems are stored in attributes `design` and `offdesign`, respectively.\n",
    "Unknowns and equations are added with methods `add_unknown` and `add_equation`:\n",
    "\n",
    "```python\n",
    "case.design  \\\n",
    "    .add_unknown('pipe.diameter')  \\  # value shared with all points\n",
    "    .add_equation('pipe.flow_in.W == 50')  # Constraint equation\n",
    "\n",
    "case.offdesign  \\\n",
    "    .add_unknown('pedal.angle')  \\  # value pertains to current point only\n",
    "    .add_equation('outlet.pressure == p_atm')  # Constraint equation\n",
    "```\n",
    "\n",
    "Note that `case.add_unknown` and `case.add_equation` are shortcuts to `case.offdesign.add_unknown` and `case.offdesign.add_equation`, respectively.\n",
    "\n",
    "Unknowns and equations defined at system `setup` define global *off-design* constraints.\n",
    "Such problems are usually imposed by physics (continuity conditions, conservation laws, *etc.*), and apply to all `RunSingleCase` drivers.\n",
    "\n",
    "The good practice is to have by default the system operating in off-design condition, and introduce design methods to design it (see [tutorial on design methods](11-DesignMethods.ipynb)).\n",
    "\n",
    "Some systems may impose off-design equations without declaring off-design unknowns, and *vice versa*.\n",
    "It is up to the user to close the mathematical problem by providing appropriate degrees of freedom and constraints, when required.\n",
    "\n",
    "Ultimately, the solver will check if the mathematical system is square before solving it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m.drivers.clear() # Remove all drivers on the system `m`\n",
    "solver = m.add_driver(NonLinearSolver('solver'))\n",
    "update = solver.add_child(RunSingleCase('update'))\n",
    "\n",
    "# Customization of the case\n",
    "update.set_values({'expected_output': 15})\n",
    "\n",
    "# Execution\n",
    "m.run_drivers()\n",
    "\n",
    "print(\n",
    "    \"Off-design default\",\n",
    "    f\"K1 = {m.K1}\",\n",
    "    f\"p_in.x = {m.p_in.x}\",\n",
    "    f\"p_out.x = {m.p_out.x}\",\n",
    "    sep=\"\\n\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Design methods are defined at system level, and can be activated on demand in a simulation (more info in the [Design Method tutorial](./11-DesignMethods.ipynb))."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MultiplyWithDesignMethod(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(XPort, 'p_in', {'x': 1.})\n",
    "        self.add_output(XPort, 'p_out', {'x': 1.})\n",
    "\n",
    "        self.add_inward('K1', 5.)\n",
    "        \n",
    "        # Off-design default\n",
    "        self.add_inward('expected_output', 7.5)\n",
    "        self.add_unknown('p_in.x').add_equation('p_out.x == expected_output')\n",
    "\n",
    "        # Design methods\n",
    "        self.add_inward('dx_design', 1.0)        \n",
    "        self.add_design_method('dx').add_unknown('K1').add_equation('p_out.x - p_in.x == dx_design')\n",
    "\n",
    "    def compute(self):\n",
    "        self.p_out.x = self.p_in.x * self.K1\n",
    "\n",
    "\n",
    "from cosapp.recorders import DataFrameRecorder\n",
    "\n",
    "m = MultiplyWithDesignMethod('m')\n",
    "m.expected_output = 7.5\n",
    "\n",
    "solver = m.add_driver(NonLinearSolver('solver'))\n",
    "solver.add_recorder(DataFrameRecorder(includes=['K1', 'p_*.x', 'expected*']))\n",
    "\n",
    "# Design\n",
    "design = solver.add_child(RunSingleCase('design'))\n",
    "design.set_values({\n",
    "    'expected_output': 7.5,\n",
    "    'dx_design': 5,\n",
    "})\n",
    "design.design.extend(m.design_methods['dx'])\n",
    "\n",
    "m.run_drivers()\n",
    "\n",
    "print(\n",
    "    \"Design:\",\n",
    "    f\"K1 = {m.K1}\",\n",
    "    f\"p_in.x = {m.p_in.x}\",\n",
    "    f\"p_out.x = {m.p_out.x}\",\n",
    "    sep=\"\\n\"\n",
    ")\n",
    "print(solver.problem)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`RunSingleCase` drivers may also be used to simulate a life cycle made of states. On next example, a design point is followed by two off-design operating points. \n",
    "\n",
    "![metadriver](images/drivers_5.svg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Off-design 1\n",
    "offdesign1 = solver.add_child(RunSingleCase('offdesign1'))\n",
    "offdesign1.set_values({'expected_output': 15.})\n",
    "\n",
    "# Off-design 2\n",
    "offdesign2 = solver.add_child(RunSingleCase('offdesign2'))\n",
    "offdesign2.set_values({'expected_output': 24.})\n",
    "\n",
    "m.run_drivers()\n",
    "\n",
    "df = solver.recorder.export_data()  # export recorded data as a pandas DataFrame\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For advanced design methods tutorial and strategy to create a \"design model\" from a \"simulation model\", we recommend the tutorial on [Design Methods](./11-DesignMethods.ipynb).\n",
    "\n",
    "**Congrats!** You are now ready to launch computation on your `System` with **CoSApp**!"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "03d8647662c9fbe9220ebb6c4a5dd3c1d557fb5efab079901b8383e5f052f0cc"
  },
  "kernelspec": {
   "display_name": "Python 3.9.12",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
