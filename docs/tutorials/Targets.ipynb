{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** examples:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Settings targets on output values\n",
    "\n",
    "CoSApp systems declare a directional interface, and transform inputs into outputs.\n",
    "It is good practice to design systems such that their inputs and outputs correspond to the \"normal\", direct behaviour of the system, without presuming how it will be used in a more complex assembly, or what users will want to do with it.\n",
    "\n",
    "As it happens, it is very common to use a given system in a reverse way, when one wishes certain outputs to reach a target value.\n",
    "\n",
    "The first obvious way to achieve that is to setup a nonlinear problem, whereby an input is sought to satisfy an equation of the kind \"target_output == target_value\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "\n",
    "class SimpleFunction(System):\n",
    "    def setup(self):\n",
    "        self.add_inward('a', 0.0)\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_outward('y', 0.0)\n",
    "    \n",
    "    def compute(self) -> None:\n",
    "        self.y = self.x**2 - self.a\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "f = SimpleFunction('f')\n",
    "\n",
    "solver = f.add_driver(NonLinearSolver('solver'))\n",
    "solver.add_unknown('x').add_equation('y == 0')\n",
    "\n",
    "f.a = 2.0\n",
    "f.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"f.x = {f.x}\",\n",
    "    f\"f.y = {f.y}\",\n",
    "    sep = \"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Displaying attribute `solver.problem` reveals the mathematical problem solved by the driver:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solver.problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set target value dynamically\n",
    "\n",
    "In the previous example, the target value for `f.y` is hard-coded as the right-hand side of the design equation.\n",
    "Changing this value requires the setup of a new mathematical problem, with a new equation.\n",
    "\n",
    "An alternative way is to this procedure is the use of method `add_target`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "f = SimpleFunction('f')\n",
    "\n",
    "solver = f.add_driver(NonLinearSolver('solver'))\n",
    "solver.add_unknown('x').add_target('y')\n",
    "\n",
    "f.a = 2.0\n",
    "f.y = 0.0   # set target value by setting output variable\n",
    "f.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"f.x = {f.x}\",\n",
    "    f\"f.y = {f.y}\",\n",
    "    sep = \"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Job done!\n",
    "Looking at attribute `problem` shows that the actual mathematical problem has not changed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solver.problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The difference, however, is that the right-hand side of the equation is now dynamically set to the current value of `f.y` before each solver execution.\n",
    "Therefore, we can now update the target value interactively, by simply assigning a new value to `f.y`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f.y = -0.5  # update target value dynamically\n",
    "\n",
    "f.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"f.x = {f.x}\",\n",
    "    f\"f.y = {f.y}\",\n",
    "    sep = \"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be seen after each computation, `f.y` only reaches the targetted value *within solver tolerance*.\n",
    "Indeed, if `add_target` offers a convenient way of defining target values, one must keep in mind that `f.y` remains an output, whose value is strictly determined by the actual inputs of `f`.\n",
    "\n",
    "As a consequence, it is up to users to control the value of targetted variables *before each solver execution*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Targets and design methods\n",
    "\n",
    "Targets can be particularly interesting to define [design methods](11-DesignMethods.ipynb) with a controllable target value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "\n",
    "class SimpleFunctionWithDesign(System):\n",
    "    def setup(self):\n",
    "        self.add_inward('a', 0.0)\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_outward('y', 0.0)\n",
    "\n",
    "        design = self.add_design_method('y')\n",
    "        design.add_unknown('x').add_target('y')\n",
    "    \n",
    "    def compute(self) -> None:\n",
    "        self.y = self.x**2 - self.a\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "f = SimpleFunctionWithDesign('f')\n",
    "\n",
    "solver = f.add_driver(NonLinearSolver('solver'))\n",
    "solver.extend(f.design_methods['y'])\n",
    "\n",
    "f.a = 2.0\n",
    "f.y = -0.5\n",
    "f.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"f.x = {f.x}\",\n",
    "    f\"f.y = {f.y}\",\n",
    "    sep = \"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Without the use of `add_target`, the only way to control the design value of `y` would be to declare an additional inward `y_target`, say, used as right-hand side value of design equation:\n",
    "```python\n",
    "    def setup(self):\n",
    "        self.add_inward('a', 0.0)\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_outward('y', 0.0)\n",
    "\n",
    "        design = self.add_design_method('y')\n",
    "        self.add_inward('y_target', 0.0)\n",
    "        design.add_unknown('x').add_equation('y == y_target')\n",
    "```\n",
    "\n",
    "While this syntax works, it has the inconvenience of burdening the system with an inward only meaningful when design method `'y'` is activated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pulling a targetted variable\n",
    "\n",
    "Consider a system with a targetted output pulled at parent level.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Composite(System):\n",
    "    def setup(self):\n",
    "        f = self.add_child(SimpleFunction('f'), pulling='x')\n",
    "        g = self.add_child(SimpleFunctionWithDesign('g'), pulling='y')\n",
    "\n",
    "        self.connect(f, g, {'y': 'a'})  # f.y -> g.a\n",
    "\n",
    "        # Promote design method of `g` at parent level\n",
    "        # Note:\n",
    "        #   g.design('y') is the same as g.design_methods['y']\n",
    "        self.add_design_method('y').extend(g.design('y'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = Composite('head')\n",
    "\n",
    "solver = head.add_driver(NonLinearSolver('solver'))\n",
    "\n",
    "solver.extend(head.design('y'))\n",
    "\n",
    "head.x = 3.0\n",
    "head.g.y = 5.0\n",
    "head.f.a = 2.0\n",
    "\n",
    "head.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"Solution:\\n\"\n",
    "    f\"head.g.x = {head.g.x}\",\n",
    "    f\"head.g.y = {head.g.y}\",\n",
    "    f\"\\n{solver.problem!r}\",\n",
    "    sep = \"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What just happened?!\n",
    "\n",
    "In this case, output `head.g.y` is pulled up at parent level. Behind the scene, an *upward* connection is created from `head.g.y` to `head.y`, such that `y` appears as a natural output of top system `head`, computed by sub-system `head.g`.\n",
    "\n",
    "As a consequence, it seems natural to set the targetted value in the context of system `head`, that is setting `head.y` instead of `head.g.y`, when activating `head.design('y')`.\n",
    "Let's try again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = Composite('head')\n",
    "\n",
    "solver = head.add_driver(NonLinearSolver('solver'))\n",
    "\n",
    "solver.extend(head.design('y'))  # activates a target on `head.y`\n",
    "\n",
    "head.x = 3.0\n",
    "head.y = 5.0    # set target value\n",
    "head.f.a = 2.0\n",
    "\n",
    "head.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"Solution:\\n\"\n",
    "    f\"head.g.x = {head.g.x}\",\n",
    "    f\"head.g.y = {head.g.y}\",\n",
    "    f\"\\n{solver.problem!r}\",\n",
    "    sep = \"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Weak and strong targets\n",
    "\n",
    "A target is said to be *weak* if it can be disregarded in certain situations.\n",
    "By default, targets are *strong*, meaning the target equation is always enforced.\n",
    "\n",
    "A weak target is discarded if the targetted variable\n",
    "\n",
    "1. is an output;\n",
    "2. is connected to an input.\n",
    "\n",
    "The second condition specifically excludes pulled outputs, which is the only admissible output-output connection.\n",
    "\n",
    "Weak targets may be useful when a targetted variable is transmitted through a chain of systems, and one wants to specify the target on the last system only.\n",
    "This is typically the case in the next example, where we simulate three resistors series, and wish to determine the current between end-point voltages.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System, Port\n",
    "\n",
    "class ElectricPort(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable(\"I\", 1.0, unit=\"A\", desc=\"Current\")\n",
    "        self.add_variable(\"V\", 0.0, unit=\"V\", desc=\"Voltage\")\n",
    "\n",
    "class Resistor(System):\n",
    "    def setup(self):\n",
    "        self.add_input(ElectricPort, 'elec_in')\n",
    "        self.add_output(ElectricPort, 'elec_out')\n",
    "\n",
    "        self.add_inward(\"R\", 1e2, unit=\"ohm\", desc=\"Resistance\")\n",
    "        self.add_outward(\"deltaV\", 0.0, unit=\"V\")\n",
    "\n",
    "        # Off-design constraint: compute current to reach target voltage\n",
    "        self.add_unknown(\"elec_in.I\").add_target(\"elec_out.V\", weak=True)\n",
    "    \n",
    "    def compute(self):\n",
    "        elec_in, elec_out = self.elec_in, self.elec_out\n",
    "        self.deltaV = self.R * elec_in.I\n",
    "        elec_out.I = elec_in.I\n",
    "        elec_out.V = elec_in.V - self.deltaV\n",
    "\n",
    "class ThreeResistorSeries(System):\n",
    "    def setup(self):\n",
    "        R1 = self.add_child(Resistor(\"R1\"), pulling=\"elec_in\")\n",
    "        R2 = self.add_child(Resistor(\"R2\"))\n",
    "        R3 = self.add_child(Resistor(\"R3\"), pulling=\"elec_out\")\n",
    "        \n",
    "        self.connect(R1.elec_out, R2.elec_in)\n",
    "        self.connect(R2.elec_out, R3.elec_in)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each resistor defines an inner off-design problem, in which current is unknown, and a target is set on the output voltage.\n",
    "When several resistors are connected, local unknown currents are discarded every time they belong to a connected input port, which occurs at each node point connecting adjacent resistors.\n",
    "Likewise, local weak targets on voltages are discarded at each connecting node, where output voltage is transmitted to the next resistor.\n",
    "\n",
    "Overall, one unknown (the incoming current into the first resistor, pulled as `elec_in.I`) and one target (the output voltage of the last resistor, pulled as `elec_out.V`) remain.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = ThreeResistorSeries('s')\n",
    "\n",
    "s.R1.R = 100\n",
    "s.R2.R = 50\n",
    "s.R3.R = 250\n",
    "\n",
    "s.elec_in.V = 10\n",
    "s.elec_out.V = -2\n",
    "\n",
    "# Set bogus target values at connection points\n",
    "# These values will be discarded, as targets are weak\n",
    "s.R1.elec_out.V = 1.23e4\n",
    "s.R2.elec_out.V = -8e17\n",
    "\n",
    "s.add_driver(NonLinearSolver('solver'))\n",
    "s.run_drivers()\n",
    "\n",
    "voltages = [s.elec_in.V]\n",
    "voltages.extend(s[f\"R{i}.elec_out.V\"] for i in range(1, 4))\n",
    "\n",
    "print(\n",
    "    f\"Solution:\\n\"\n",
    "    f\"s.elec_in.I = {s.elec_in.I}\",\n",
    "    f\"s.elec_in.V = {s.elec_in.V}\",\n",
    "    f\"s.elec_out.V = {s.elec_out.V}\",\n",
    "    f\"Voltages = {voltages}\",\n",
    "    f\"\\nOverall resistance: {(s.elec_in.V - s.elec_out.V) / s.elec_in.I :.2f} Ohm\",\n",
    "    sep = \"\\n\",\n",
    ")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.drivers['solver'].problem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import plotly.express as px\n",
    "\n",
    "df = pd.DataFrame.from_dict(\n",
    "    {\n",
    "        \"V\": voltages,\n",
    "        \"index\": list(range(len(voltages))),\n",
    "        \"node\": list('ABCD'),\n",
    "    }\n",
    ")\n",
    "\n",
    "fig = px.line(df, x=\"node\", y=\"V\",\n",
    "    title=\"Voltage profile\",\n",
    ")\n",
    "fig.update_layout(\n",
    "    height=450,\n",
    "    hovermode='x',\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Targetted expressions\n",
    "\n",
    "A target can also be set on an evaluable expression, such as `norm(v)` or `y * (y + 1)`, as long as the expression only involves a single variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class CubicFunction(System):\n",
    "    def setup(self):\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_outward('y', 0.0)\n",
    "    \n",
    "    def compute(self) -> None:\n",
    "        self.y = self.x**3\n",
    "\n",
    "f = CubicFunction('f')\n",
    "\n",
    "solver = f.add_driver(NonLinearSolver('solver'))\n",
    "\n",
    "solver.add_unknown('x').add_target('abs(y)')\n",
    "\n",
    "f.y = 8  # value used for targetted expression\n",
    "f.x = 4  # positive initial value of unknown\n",
    "f.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"f.x = {f.x}\",\n",
    "    f\"f.y = {f.y}\",\n",
    "    sep = \"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Same computation, with a different initial guess on `f.x`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f.y = 8\n",
    "f.x = -5  # negative initial value\n",
    "f.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"f.x = {f.x}\",\n",
    "    f\"f.y = {f.y}\",\n",
    "    sep = \"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "\n",
    "f = CubicFunction('f')\n",
    "\n",
    "solver = f.add_driver(NonLinearSolver('solver'))\n",
    "\n",
    "try:\n",
    "    solver.add_unknown('x').add_target('2 * y + cos(pi * x)')\n",
    "except Exception as error:\n",
    "    logging.exception(error)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
