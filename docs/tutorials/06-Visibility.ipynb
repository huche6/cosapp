{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials: Data visibility"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visibility ![Experimental feature](images/experimental.svg)\n",
    "\n",
    "Model users and model developers are usually distinct persons. Therefore, users may not be aware of model limitations. Moreover, only a few model parameters may be meaningful to them. \n",
    "\n",
    "To address these issues, **CoSApp** allows model developers to specify the validity range and visibility scope of all variables.\n",
    "This section addresses the visibility scope.\n",
    "\n",
    "## The concept\n",
    "\n",
    "Three levels of visibility are available in **CoSApp**:\n",
    "\n",
    "* `PUBLIC`: Everybody can set those variables\n",
    "* `PROTECTED`: Only advanced and expert users may set those variables\n",
    "* `PRIVATE`: Only expert users may set those variables\n",
    "\n",
    "The accessibility is the intersection between the `System` tags and the `User` role:\n",
    "\n",
    "* `PUBLIC`: no shared tag\n",
    "* `PROTECTED`: more than one shared tag (but not all)\n",
    "* `PRIVATE`: same tags list\n",
    "\n",
    "![visibility](../tutorials/images/visibility.svg)\n",
    "\n",
    "## Example\n",
    "\n",
    "Consider the mechanical and aerodynamic design of a turbine blade, involving three specialists: a mechanical engineer, an aerodynamics expert and a system engineer.\n",
    "\n",
    "The models for such study could concievably define the following variable scopes:\n",
    "\n",
    "```python\n",
    "class MechanicalBlade(System):\n",
    "    \n",
    "    tags = ['blade', 'mechanics']\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_inward('height', 0.4, scope=Scope.PUBLIC)\n",
    "        self.add_inward('thickness', 0.01, scope=Scope.PROTECTED)\n",
    "        self.add_inward('material', 'steel')  # Scope is PRIVATE by default\n",
    "\n",
    "\n",
    "class AerodynamicBlade(System):\n",
    "    \n",
    "    tags = ['blade', 'aerodynamics']\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_inward('height', 0.4, scope=Scope.PUBLIC)\n",
    "        self.add_inward('thickness', 0.01, scope=Scope.PROTECTED)\n",
    "        self.add_inward('camber', 1e-3)  # Scope is PRIVATE by default\n",
    "\n",
    "\n",
    "class Blade(System):\n",
    "    \n",
    "    tags = ['blade', 'integration']\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_child(MechanicalBlade('mechanics'), pulled={'height': 'height'})\n",
    "        self.add_child(AerodynamicBlade('aerodynamic'), pulled={'height': 'height'})\n",
    "```\n",
    "\n",
    "Users will be assigned the following tags:\n",
    "\n",
    "| User | Role |\n",
    "|---|---|\n",
    "| Mechanical Engineer | [\"blade\", \"mechanics\"] |\n",
    "| Aerodynamics Engineer | [\"blade\", \"aerodynamics\"] |\n",
    "| System Engineer | [\"integration\"] |\n",
    "\n",
    "\n",
    "Variable credentials for the three specialists will then be:\n",
    "\n",
    "| User | MechanicalBlade | AerodynamicBlade | Blade |\n",
    "|---|---|---|---|\n",
    "| Mechanical Engineer | PRIVATE | PROTECTED | PROTECTED |\n",
    "| Aerodynamics Engineer | PROTECTED | PRIVATE | PROTECTED |\n",
    "| System Engineer | PUBLIC | PUBLIC | PROTECTED |\n",
    "\n",
    "\n",
    "## Defining visibility\n",
    "\n",
    "The example highlights the link between a user's role and their ability to set a particular variable.\n",
    "\n",
    "User roles are currently saved in a configuration file (*%USERPROFILE%\\\\.cosapp.d\\\\cosapp_config.json* on Windows, and *\\$HOME/.cosapp.d/cosapp_config.json* on Linux/Unix). A role is defined as a list of tags, akin to user groups in Unix OS.\n",
    "For example, the Aerodynamics Engineer will be assigned the following roles: \n",
    "\n",
    "```json\n",
    "{\n",
    "  \"roles\" : [\n",
    "    [\"aerodynamics\", \"rotor\"],\n",
    "    [\"aerodynamics\", \"stator\"]\n",
    "  ]\n",
    "}\n",
    "```\n",
    "\n",
    "The variable visibility for a given user is determined by comparing user roles with the `tags` of each `System`. If one role matches exactly the System tags, the user will have `PRIVATE` clearance on the `System`. If one role tag matches at least one system tag, the user will have `PROTECTED` clearance. Otherwise the user will have `PUBLIC` access.\n",
    "\n",
    "Visibility of validity ranges is set at `Port` or `System` level. However, unlike validity parameters, it is not possible to modify a `Port` variable visibility inside a `System`.\n",
    "\n",
    "For example, in a `Port`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "logging.getLogger().setLevel(logging.INFO)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System, Port, Scope\n",
    "\n",
    "class MyPort(Port):\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_variable('v', 22.)  # Scope is PUBLIC by default\n",
    "        self.add_variable('w', 22., scope=Scope.PRIVATE)\n",
    "        self.add_variable('x', 22., scope=Scope.PROTECTED)\n",
    "        self.add_variable('y', 22., scope=Scope.PUBLIC)        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the purpose of ports is the exchange of information between systems, it should not be common to define a scope on them. Therefore the default visibility of port variables is `PUBLIC`.\n",
    "\n",
    "In a `System`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MechanicalBlade(System):\n",
    "    \n",
    "    tags = ['blade', 'mechanics']  # Tags must be specified to activate visibility, otherwise all variables will be open.\n",
    "    \n",
    "    def setup(self):\n",
    "        self.add_inward('height', 0.4, scope=Scope.PUBLIC)\n",
    "        self.add_inward('thickness', 0.01, scope=Scope.PROTECTED)\n",
    "        self.add_inward('material', 'steel')  # Scope is PRIVATE by default\n",
    "        \n",
    "        port_in = self.add_input(MyPort, 'port_in')\n",
    "        # Visibility of variable v in port_in cannot be modified\n",
    "        \n",
    "        port_out = self.add_output(MyPort, 'port_out')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Inwards are the preferred variables to define a restrained visibility. Hence, their default scope is `PRIVATE`.\n",
    "\n",
    "## Displaying visibility\n",
    "\n",
    "To obtain the documentation of a `System` or a `Port`, you can use the utility function `display_doc`. \n",
    "\n",
    "Variables with `PRIVATE` scope will be marked with symbols 🔒🔒, and `PROTECTED` ones with 🔒."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.tools import display_doc\n",
    "\n",
    "display_doc(MyPort)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display_doc(MechanicalBlade)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing visibility\n",
    "\n",
    "If a variable has a scope other than `PUBLIC`, users will not be able to set it except if they have the right role.\n",
    "\n",
    "Only *inputs* and *inwards* may be protected, as *outputs* and *outwards* are overwritten at each `System` execution.\n",
    "\n",
    "So assuming you are not assigned roles `['blade', 'runner']`, here are the variables you cannot touch:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import ScopeError\n",
    "\n",
    "BladeRunner = MechanicalBlade  # Class duplication\n",
    "BladeRunner.tags = ['blade', 'runner']  # Changing the tags on the new class\n",
    "\n",
    "b = BladeRunner('Ridley')\n",
    "\n",
    "from warnings import warn\n",
    "\n",
    "try:\n",
    "    b.thickness = 0.01\n",
    "except ScopeError as err:\n",
    "    warn(f\"ScopeError raised: {err}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    b.material = 0.01\n",
    "except ScopeError as err:\n",
    "    warn(f\"ScopeError raised: {err}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    b.port_in.w = 0.01\n",
    "except ScopeError as err:\n",
    "    warn(f\"ScopeError raised: {err}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, visibility on output port is not enforced:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b.port_out.w = 0.01\n",
    "print(b.port_out)"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  },
  "toc": {
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": "block",
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
