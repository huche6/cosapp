{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Surrogate models - Advanced features\n",
    "\n",
    "A deeper dive into meta-models created by `make_surrogate`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Update of sub-system variables at each surrogate model execution\n",
    "\n",
    "As discussed in the tutorial on [surrogate models](./SystemSurrogates.ipynb), a meta-model, when activated, supersedes the behaviour of a system as originally defined by its `compute()` method. The meta-model is a black box whose inputs are specified by the Design of Experiment (DOE) on which it was trained. But what about its outputs?\n",
    "\n",
    "At the very least, the output of a system surrogate should match those of the original system.\n",
    "CoSApp runs the extra mile, by adding all outputs *and* connected inputs of all sub-systems to the list of meta-model outputs. Moreover, at each surrogate model execution, all sub-system variables are synchronized with the computed meta-model outputs.\n",
    "\n",
    "This way, the meta-modeled system *and its sub-systems* are kept in a state consistent with its free inputs.\n",
    "\n",
    "Consider the systems defined in the tutorial on [surrogate models](./SystemSurrogates.ipynb):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System, Port\n",
    "\n",
    "\n",
    "class FloatPort(Port):\n",
    "    \"\"\"Simple port containing a single float variable\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_variable('value', 0.0)\n",
    "\n",
    "\n",
    "class MultiplyByM(System):\n",
    "    \"\"\"System computing y = m * x\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_inward('m', 1.0)\n",
    "        self.add_input(FloatPort, 'x')\n",
    "        self.add_output(FloatPort, 'y')\n",
    "    \n",
    "    def compute(self):\n",
    "        self.y.value = self.m * self.x.value\n",
    "\n",
    "\n",
    "class AffineSystem(System):\n",
    "    \"\"\"System computing out = a.m * b.m * x + 3\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_output(FloatPort, 'out')\n",
    "        # sub-systems\n",
    "        self.add_child(MultiplyByM('a'), pulling='x')\n",
    "        self.add_child(MultiplyByM('b'))\n",
    "        # connections\n",
    "        self.connect(self.a.y, self.b.x)\n",
    "    \n",
    "    def compute(self):\n",
    "        self.out.value = self.b.y.value + 3\n",
    "\n",
    "    @property\n",
    "    def expected_output(self) -> float:\n",
    "        \"\"\"Expected value for `self.out.value`\"\"\"\n",
    "        return self.a.m * self.b.m * self.x.value + 3\n",
    "       "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas\n",
    "import itertools\n",
    "\n",
    "def Cartesian_DoE(axes: dict) -> pandas.DataFrame:\n",
    "    \"\"\"Simple Cartesian grid DoE from 1D samples in all axis directions\"\"\"\n",
    "    return pandas.DataFrame(\n",
    "        list(itertools.product(*axes.values())),\n",
    "        columns = list(axes.keys()),\n",
    "    )\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "\n",
    "axes = {\n",
    "    'x.value': numpy.linspace(-5, 5, 11),\n",
    "    'a.m': numpy.linspace(-3, 3, 6),\n",
    "    'b.m':numpy.linspace(-3, 3, 6),\n",
    "}\n",
    "\n",
    "doe = Cartesian_DoE(axes)\n",
    "\n",
    "head = AffineSystem('head')\n",
    "meta = head.make_surrogate(doe)\n",
    "\n",
    "print(\n",
    "    f\"Outputs of {head.name!r}:\",\n",
    "    dict(filter(lambda item: len(item[1]) > 0, head.outputs.items())),  # non-empty output ports\n",
    "    f\"\\nOutputs of meta-model:\",\n",
    "    meta.synched_outputs,\n",
    "    sep='\\n',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be seen in previous cell, system `head` has only one output `out.value`, whereas `meta` has five, the last four of which are sub-system variables.\n",
    "Every time `head` is executed, its meta-model not only updates `head.out.value`, but also `head.a` and `head.b` variables, as would be the case with the original system.\n",
    "\n",
    "In cell below, we show the internal state of system `head` with or without meta-model bypass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def run_and_print(head, activate):\n",
    "    inputs = ['x.value']\n",
    "    outputs = ['out.value', 'a.x.value', 'a.y.value', 'b.x.value', 'b.y.value']\n",
    "    for var in outputs:\n",
    "        head[var] = -999.999  # set to bogus value\n",
    "    # Set surrogate status and run model\n",
    "    head.active_surrogate = activate\n",
    "    head.run_once()\n",
    "    # Print info on internal variables\n",
    "    print('\\nActivated meta-model:', head.active_surrogate)\n",
    "    for var in inputs + outputs:\n",
    "        print(f\"{var}:\", head[var], sep='\\t')\n",
    "\n",
    "head.x.value = 3.14\n",
    "head.a.m = 0.25\n",
    "head.b.m = -1\n",
    "\n",
    "run_and_print(head, activate=False)\n",
    "run_and_print(head, activate=True)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After meta-model activation, top-level output `out.value` is estimated with a numerical error.\n",
    "\n",
    "Noticeably, it also appears that internal connections are accounted for by the meta-model, as `a.y.value` and `b.x.value` are equal, for example.\n",
    "Moreover, the behaviour $y = m\\,x$ is also satisfied (within numerical error) for sub-systems `a` and `b`, with `head.a.m = 0.25` and `head.b.m = -1`.\n",
    "Precision is rather poor here, because the meta-model was trained on scarce data.\n",
    "\n",
    "Post-synchronization can be altered at meta-model creation, with optional argument `postsynch`, specifying a name pattern (or a list thereof) for variables that must be synchronized. By default, this argument is set to `'*'`, meaning *\"synchronize everything\"*. An empty list or `None` value means *\"do not synchronize anything apart from top-level outputs\"*.\n",
    "\n",
    "In the example below, we specify that only variables of sub-system `head.a` should be synchronized:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = AffineSystem('head')\n",
    "meta = head.make_surrogate(doe, postsynch='a.*')\n",
    "\n",
    "print(\n",
    "    f\"Outputs of meta-model:\",\n",
    "    meta.synched_outputs,\n",
    "    sep='\\n',\n",
    ")\n",
    "head.x.value = 3.14\n",
    "head.a.m = 0.25\n",
    "head.b.m = -1\n",
    "\n",
    "run_and_print(head, activate=False)\n",
    "run_and_print(head, activate=True)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As requested, we observe that ports `b.x` and `b.y` have not been modified after meta-model execution.\n",
    "\n",
    "Limiting post-synchronization may also help reduce training time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training a sub-system\n",
    "\n",
    "Methods\n",
    "\n",
    "* `make_surrogate`\n",
    "* `dump_surrogate`\n",
    "* `load_surrogate`\n",
    "* `active_surrogate`\n",
    "\n",
    "can be called at any system level.\n",
    "\n",
    "Systems above the meta-modeled module (*i.e.* parent systems) will be unaffected by the procedure. Systems below (child systems), on the other hand, will automatically be deactivated, together with their drivers, if any.\n",
    "\n",
    "In the example below, we create a meta-model on sub-system `head.a` alone:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = AffineSystem('head')\n",
    "\n",
    "axes = {\n",
    "    'm': numpy.linspace(-2, 2, 11),\n",
    "    'x.value': numpy.linspace(-10, 10, 21),\n",
    "}\n",
    "\n",
    "doe = Cartesian_DoE(axes)\n",
    "head.a.make_surrogate(doe, activate=False)  # create meta-model, but do not activate it\n",
    "\n",
    "# Sanity check:\n",
    "assert not head.has_surrogate\n",
    "assert head.a.has_surrogate\n",
    "\n",
    "head.x.value = 5.2\n",
    "head.a.m = 1.55\n",
    "\n",
    "head.run_drivers()\n",
    "print(f\"head.out.value = {head.out.value} (before activation)\")\n",
    "\n",
    "head.a.active_surrogate = True\n",
    "head.run_drivers()\n",
    "print(f\"head.out.value = {head.out.value} (after activation)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Impact of training space\n",
    "\n",
    "What happens when the meta-model is trained on a subset of inputs?\n",
    "\n",
    "In the example below, we again create a meta-model for sub-system `head.a`, but do not include input `m` in the DOE:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = AffineSystem('head')\n",
    "head.a.m = 1.55\n",
    "\n",
    "axes = {\n",
    "    # 'm': numpy.linspace(-2, 2, 11),  # do not train on `m`\n",
    "    'x.value': numpy.linspace(-10, 10, 21),\n",
    "}\n",
    "\n",
    "doe = Cartesian_DoE(axes)\n",
    "head.a.make_surrogate(doe, activate=False)  # create meta-model, but do not activate it\n",
    "\n",
    "head.x.value = 5.2\n",
    "\n",
    "head.run_drivers()\n",
    "print(f\"head.out.value = {head.out.value} (before activation)\")\n",
    "\n",
    "head.a.active_surrogate = True\n",
    "head.run_drivers()\n",
    "print(f\"head.out.value = {head.out.value} (after activation)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the surrogate model was trained assuming `head.a.m` is constant and equals 1.55. The result after activation is correct, as `head.a.m` was unchanged.\n",
    "\n",
    "However, results are incorrect when this parameter is modified:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head.a.m -= 0.72\n",
    "\n",
    "head.run_drivers()\n",
    "print(f\"head.out.value = {head.out.value} instead of {head.expected_output:.15}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Tip:** Before creating a system surrogate, be sure to lookup variables that may have an impact on outputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = head.a  # system of interest\n",
    "\n",
    "print(\"Input ports:\", s.inputs, sep='\\n')\n",
    "print(\"\\nTransients and unknowns:\", list(s.unknowns), list(s.transients))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Be aware that this technique does not report sub-system inputs that may influence outputs, though."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Surrogate models and solvers\n",
    "\n",
    "Good practices when the system of interest has unknowns.\n",
    "\n",
    "Consider a class `ClosedSystem`, assembling one sub-system `usys` (of type `UnknownSystem`) bearing an unknown, and one sub-system `esys` (of type `EquationSystem`) containing an equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class UnknownSystem(System):\n",
    "    def setup(self):\n",
    "        self.add_inward('m', 1.0, desc=\"Multiplying factor\")\n",
    "        self.add_input(FloatPort, 'x')\n",
    "        self.add_output(FloatPort, 'y')\n",
    "        # Unknowns\n",
    "        self.add_unknown('m')\n",
    "\n",
    "    def compute(self):\n",
    "        self.y.value = self.x.value * self.m\n",
    "\n",
    "class EquationSystem(System):\n",
    "    def setup(self):\n",
    "        self.add_input(FloatPort, 'p')\n",
    "        self.add_property('p_target', 25)\n",
    "        self.add_equation(\"p.value == p_target\")\n",
    "\n",
    "class ClosedSystem(System):\n",
    "    def setup(self):\n",
    "        self.add_child(UnknownSystem('usys'), pulling='x')\n",
    "        self.add_child(EquationSystem('esys'))\n",
    "        \n",
    "        self.connect(self.usys.y, self.esys.p)\n",
    "\n",
    "    @property\n",
    "    def solution(self) -> float:\n",
    "        \"\"\"Exact solution for self.usys.m\"\"\"\n",
    "        try:\n",
    "            return self.esys.p_target / self.x.value\n",
    "        except ZeroDivisionError:\n",
    "            return numpy.sign(self.esys.p_target) * numpy.inf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "from cosapp.utils.surrogate_models import (\n",
    "    FloatKrigingSurrogate,\n",
    "    LinearNearestNeighbor,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Case 1: The system of interest is well-posed\n",
    "\n",
    "In this part, the system of interest is the top assembly, mathematically closed.\n",
    "\n",
    "If the system has a `NonLinearSolver` driver *before* the generation of the meta-model, CoSApp automatically tracks unknowns, and you don't have to add them to the input training dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "head = ClosedSystem(\"head\")\n",
    "head.add_driver(NonLinearSolver('solver', tol=1e-9))\n",
    "# Check that assembly system + driver can be solved\n",
    "assert head.is_standalone()\n",
    "\n",
    "doe = Cartesian_DoE({\n",
    "    'x.value': numpy.linspace(0.1, 10, 20),\n",
    "})\n",
    "meta = head.make_surrogate(doe, model=LinearNearestNeighbor)\n",
    "\n",
    "# Check that `usys.m` is tracked by meta-model\n",
    "print(meta.synched_outputs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.graph_objects as go\n",
    "from plotly.subplots import make_subplots\n",
    "\n",
    "def make_figure(head, meta=None, x_values=numpy.linspace(3, 10, 21)):\n",
    "    \"\"\"\n",
    "    Utility function to compare the behaviour of a `ClosedSystem`\n",
    "    instance `head` with and without meta-modeling on head[meta].\n",
    "    \"\"\"\n",
    "    fig = make_subplots(\n",
    "        rows=1, \n",
    "        cols=2,\n",
    "        subplot_titles=(\"Solution\", \"Relative difference\"),\n",
    "    )\n",
    "\n",
    "    system = head if meta is None else head[meta]\n",
    "    meta_results = numpy.zeros_like(x_values)\n",
    "    normal_results = numpy.zeros_like(x_values)\n",
    "\n",
    "    for k, head.x.value in enumerate(x_values):\n",
    "        system.active_surrogate = True\n",
    "        head.run_drivers()\n",
    "        meta_results[k] = head.usys.m\n",
    "\n",
    "        system.active_surrogate = False\n",
    "        head.run_drivers()\n",
    "        normal_results[k] = head.usys.m\n",
    "\n",
    "    ##################################### BUILDING FIGURE ##################################\n",
    "\n",
    "    def add_scatter(figure, row, col, y, **options):\n",
    "        figure.add_trace(\n",
    "            go.Scatter(x=x_values, y=y, **options),\n",
    "            row=row, col=col,\n",
    "        )\n",
    "\n",
    "    add_scatter(fig, 1, 1, meta_results,\n",
    "        name='Meta-model on',\n",
    "    )\n",
    "\n",
    "    add_scatter(fig, 1, 1, normal_results,\n",
    "        name='Meta-model off',\n",
    "        mode='markers',\n",
    "    )\n",
    "\n",
    "    add_scatter(fig, 1, 2,\n",
    "        y = numpy.absolute(meta_results / normal_results - 1),\n",
    "        name='Relative diff',\n",
    "        mode='markers',\n",
    "    )\n",
    "\n",
    "    ############################### GRAPH LAYOUT ########################################\n",
    "    sysname = f\"{head.name}\" if meta is None else f\"{head.name}.{system.name}\"\n",
    "    fig.update_layout(\n",
    "        title=dict(\n",
    "            text=f\"Solution with and without meta-model on {sysname!r}\",\n",
    "            y=0.95,\n",
    "            x=0.5,\n",
    "            xanchor='center',\n",
    "            yanchor='top',\n",
    "        ),\n",
    "        legend_title_text='Legend:',\n",
    "        hovermode='x',\n",
    "    )\n",
    "    fig.update_xaxes(title_text=\"head.x.value\", row=1, col=1)\n",
    "    fig.update_xaxes(title_text=\"head.x.value\", row=1, col=2)\n",
    "    fig.update_yaxes(title_text=\"head.usys.u\", row=1, col=1)\n",
    "    fig.update_yaxes(title_text=\"|meta / normal - 1|\", row=1, col=2)\n",
    "    fig.update_traces(showlegend=True)\n",
    "    \n",
    "    return fig\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = make_figure(head)\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Case 2: The system of interest is ill-posed\n",
    "\n",
    "Here, we assume that the system of interest has an unbalanced number of unknowns and equations, such that it cannot be solved on its own.\n",
    "\n",
    "This is the case of sub-system `usys` of assembly `ClosedSystem` defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = ClosedSystem(\"head\")\n",
    "head.add_driver(NonLinearSolver('solver', tol=1e-9))\n",
    "\n",
    "doe = Cartesian_DoE({\n",
    "    'inwards.m': numpy.linspace(-10, 10, 21),\n",
    "    'x.value': numpy.linspace(-10, 10, 20),\n",
    "})\n",
    "head.usys.make_surrogate(doe, activate=False)\n",
    "\n",
    "assert head.is_standalone()  # top assembly is solvable\n",
    "assert not head.usys.is_standalone()  # sub-system `usys` is not\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head.x.value = 4.\n",
    "\n",
    "print(\"Exact solution: head.usys.m =\", head.solution)\n",
    "\n",
    "def solve_and_print(activate: bool):\n",
    "    head.usys.active_surrogate = activate\n",
    "    head.run_drivers()\n",
    "    print(\"\\nMeta-model activated:\", head.usys.active_surrogate)\n",
    "    print(\"Solution for 'head.usys.m':\", head.usys.m, sep='\\t')\n",
    "    print(\"Error on 'head.esys.p.value':\", head.esys.p.value - head.esys.p_target, sep='\\t')\n",
    "\n",
    "solve_and_print(activate=False)\n",
    "solve_and_print(activate=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = make_figure(head, meta='usys')\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Depending on model type (`FloatKrigingSurrogate`, `LinearNearestNeighbor`...), the training precision for your unknowns will vary. If the DoE space is not sufficiently large, `NonLinearSolver` may fail.\n",
    "\n",
    "As a general rule of thumb, it is better to have an idea of the solution, so you can train in a range where `NonLinearSolver` is not likely to fail."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Common Pitfall"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = ClosedSystem(\"head\")\n",
    "\n",
    "doe = Cartesian_DoE({\n",
    "    # 'inwards.m': numpy.linspace(-5, 5, 11),  # `m` left out on purpose\n",
    "    'x.value': numpy.linspace(-10, 10, 21),\n",
    "})\n",
    "head.usys.make_surrogate(doe)\n",
    "\n",
    "solver = head.add_driver(NonLinearSolver('solver'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As `m` is not part of the training set, the surrogate model cannot predict its impact on outputs.\n",
    "\n",
    "`NonLinearSolver` will fail:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head.run_drivers()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.8-final"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
