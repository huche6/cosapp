{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Systems\n",
    "\n",
    "## Simple Systems\n",
    "\n",
    "A simple `System` does not contain sub-systems.\n",
    "\n",
    "### Import CoSApp core package"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import cosapp base classes\n",
    "from cosapp.base import System, Port"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** Base classes `System` and `Port` may also be imported from modules `cosapp.systems` and `cosapp.ports`, respectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a simple system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class DemoPort(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable('x')\n",
    "\n",
    "class Multiply(System):\n",
    "\n",
    "    def setup(self):\n",
    "        \"\"\"Defines system structure\"\"\"\n",
    "        self.add_input(DemoPort, 'p_in')    # define a new input port\n",
    "        self.add_output(DemoPort, 'p_out')  # define a new output port\n",
    "        # Solitary variables can also be added,\n",
    "        # as either `inward` or `outward` variables:\n",
    "        self.add_inward('K1', 1.0)\n",
    "        self.add_outward('delta_x', 0.0)\n",
    "\n",
    "    def compute(self): # `compute` method defines what the system does\n",
    "        self.p_out.x = self.p_in.x * self.K1\n",
    "        self.delta_x = self.p_out.x - self.p_in.x\n",
    "\n",
    "s = Multiply(name='mult')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Multiple systems](images/systems_1.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the `system` to confirm the expected behaviour"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.p_in.x = 10.\n",
    "s.K1 = 5.\n",
    "s.run_once()\n",
    "\n",
    "s.p_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More information on ports (`inputs` *and* `outputs`) can be found in a dedicated [Port tutorial](02-Ports.ipynb).\n",
    "\n",
    "#### `inwards` and `outwards` ports\n",
    "\n",
    "Inwards and outwards are orphan variables, which are not declared within a dedicated `Port` class.\n",
    "Instead, they are declared directly in `System.setup`, with `add_inward` and `add_outward` (for input and output variables, respectively).\n",
    "These orphan variables are dynamically added to special ports of `System`, called `inwards` and `outwards`.\n",
    "\n",
    "Thus, every system has `inwards` and `outwards` ports of different sizes and contents. All other ports possess a fixed number of variables, each with a predefined type.\n",
    "\n",
    "Typically:\n",
    "\n",
    "- An *inward* is an input parameter needed by the system to compute its output. For example, the pressure losses coefficient of a duct is an inward, used to compute the output flow port from its input flow port.\n",
    "- An *outward* is an output variable deduced from inputs, such as an intermediate variable of a computation, which is exposed to other systems. For instance, the difference between the input and output pressures in a duct system is a local variable, which may be of interest to neighbouring systems. Another example is the table object read from a filename (the filename being usually an *inward*). \n",
    "\n",
    "An inward `x` (resp. outward) defined in system `s` can be accessed as either `s.x` or `s.inwards.x` (resp. `s.outwards.x`).\n",
    "\n",
    "All variables in **CoSApp** accept additional information:\n",
    "\n",
    "- *unit*: Physical unit of the variable, given by a string. **CoSApp** will take care of unit conversion between connected systems. However, *units are not enforced* inside a `System`. Therefore, module developers must ensure that all variables are consistently converted in method `compute`.\n",
    "- *desc*: Short description of the variable.\n",
    "- *dtype*: If you need to force certain data type(s) on a variable, a tuple of acceptable types can be provided through this keyword. If that information is not supplied, dtype is inferred from the variable value; *e.g.* a number (`int` or `float`) will be typed as `Number`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MultiplyAdvanced(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(DemoPort, 'p_in')\n",
    "        # Inward and outward variables accept optional dtype and unit\n",
    "        self.add_inward('K1', 1, dtype=int, desc='Multiplication coefficient')\n",
    "        self.add_outward('delta_x',\n",
    "            value = 0.0,\n",
    "            unit = 'Pa',\n",
    "            dtype = (int, float), \n",
    "            desc = \"Difference between `DemoPort` output and input\"\n",
    "        )\n",
    "        self.add_output(DemoPort, 'p_out')\n",
    "\n",
    "    def compute(self):\n",
    "        self.p_out.x = self.p_in.x * self.K1\n",
    "        self.delta_x = self.p_out.x - self.p_in.x\n",
    "\n",
    "advanced = MultiplyAdvanced(name='mult')\n",
    "\n",
    "print(\n",
    "    f\"Inwards: \\n  {advanced.inwards}\",\n",
    "    f\"Outwards:\\n  {advanced.outwards}\",\n",
    "    sep=\"\\n\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Composite Systems\n",
    "\n",
    "Composite systems contain sub-systems, referred to as `children`.\n",
    "\n",
    "Sub-systems are added to a composite system using method `add_child`.\n",
    "Connections between child systems are declared at parent level, with method `connect`.\n",
    "The typical syntax is `parent.connect(child1.portA, child2.portB)`.\n",
    "\n",
    "`Port` connections are described in detail in the [Port tutorial](02-Ports.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MultiplyComplexSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        # Children\n",
    "        self.add_child(Multiply('mult1'))  # add a sub-system\n",
    "        self.add_child(Multiply('mult2'))\n",
    "        \n",
    "        # Connectors\n",
    "        self.connect(self.mult1.p_out, self.mult2.p_in)  # connect ports of sub-systems\n",
    "\n",
    "head = MultiplyComplexSystem(name='head')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Connection between Systems](images/systems_2.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the `system` to confirm the expected behaviour"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head.mult1.p_in.x = 10.\n",
    "head.mult1.K1 = 5.\n",
    "head.mult2.K1 = 5.\n",
    "head.run_once()\n",
    "\n",
    "print(f\"head.mult1.p_in.x = {head.mult1.p_in.x}\")\n",
    "print(f\"head.mult2.p_out.x = {head.mult2.p_out.x}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connect systems\n",
    "\n",
    "As illustrated in the previous example, ports of two sibling systems can be connected by `System.connect`.\n",
    "Alternatively, systems can be connected directly, with a port or a variable mapping specifying which parts of the two systems should be connected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class CompositeSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        a = self.add_child(Multiply('a'))\n",
    "        b = self.add_child(Multiply('b'))\n",
    "        \n",
    "        self.connect(a, b, {'p_out.x': 'K1'})  # connect `b.K1` to `a.p_out.x`\n",
    "\n",
    "head = CompositeSystem(name='head')\n",
    "head.a.p_in.x = 1.2\n",
    "head.a.K1 = 5.\n",
    "head.b.p_in.x = 0.5\n",
    "head.b.K1 = 2.  # will be overwritten\n",
    "head.run_once()\n",
    "\n",
    "print(f\"head.a.p_in.x = {head.a.p_in.x}\")\n",
    "print(f\"head.a.p_out.x = {head.a.p_out.x}\")\n",
    "print(f\"head.b.p_out.x = {head.b.p_out.x}\")\n",
    "print(f\"head.b.K1 = {head.b.K1}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** Full system connections are forbidden, as they can be ambiguous:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "from cosapp.base import ConnectorError\n",
    "\n",
    "try:\n",
    "    head.connect(head.a, head.b)  # full system connection\n",
    "\n",
    "except ConnectorError as error:\n",
    "    logging.error(error)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connection between system levels\n",
    "\n",
    "In above example, we need to know the internal system architecture to access port `head.mult1.p_in`. Instead, we may want to promote `p_in` at parent level, as an important system port, and access it as `head.p_in`.\n",
    "\n",
    "This can be achieved with parent-child connectors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MultiplyComplexSystem1(System):\n",
    "\n",
    "    def setup(self):\n",
    "        # inputs / outputs\n",
    "        self.add_input(DemoPort, 'p_in')\n",
    "        self.add_output(DemoPort, 'p_out')\n",
    "\n",
    "        # Children\n",
    "        self.add_child(Multiply('mult1'))\n",
    "        self.add_child(Multiply('mult2'))\n",
    "        \n",
    "        # Connections between siblings\n",
    "        self.connect(self.mult1.p_out, self.mult2.p_in)\n",
    "\n",
    "        # Parent-child connections\n",
    "        self.connect(self.p_in, self.mult1.p_in)\n",
    "        self.connect(self.p_out, self.mult2.p_out)\n",
    "\n",
    "head = MultiplyComplexSystem1(name='head')\n",
    "\n",
    "head.p_in.x = 10.\n",
    "head.mult1.K1 = 5.\n",
    "head.mult2.K1 = 5.\n",
    "head.run_once()\n",
    "\n",
    "print('head.p_in:', head.p_in)\n",
    "print('head.p_out:', head.p_out)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Connection between Systems with system view](images/systems_4.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The exact same thing can be achieved more conveniently with option `pulling`, in `add_child`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MultiplyComplexSystem2(System):\n",
    "\n",
    "    def setup(self):\n",
    "        # Children\n",
    "        self.add_child(Multiply('mult1'), pulling='p_in')   # expose `p_in` as parent input\n",
    "        self.add_child(Multiply('mult2'), pulling='p_out')  # expose `p_out` as parent output\n",
    "        \n",
    "        # Connectors\n",
    "        self.connect(self.mult1.p_out, self.mult2.p_in)\n",
    "\n",
    "head = MultiplyComplexSystem2(name='head')\n",
    "\n",
    "head.p_in.x = 10.\n",
    "head.mult1.K1 = 5.\n",
    "head.mult2.K1 = 5.\n",
    "head.run_once()\n",
    "\n",
    "print('head.p_in:', head.p_in, sep=\"\\t\")\n",
    "print('head.p_out:', head.p_out, sep=\"\\t\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When more that one port need to be pulled up, a list or a tuple of ports may be provided, as in `pulling=['portA', 'portB']`.\n",
    "It is also possible to change the name at parent level, by providing a name mapping through a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MultiplyComplexWithPulling(System):\n",
    "\n",
    "    def setup(self):\n",
    "        # Children\n",
    "        self.add_child(Multiply('mult1'), pulling={'K1': 'K11', 'p_in': 'p_in'})    # `mult1.K1` mapped as `self.K11`\n",
    "        self.add_child(Multiply('mult2'), pulling={'K1': 'K12', 'p_out': 'p_out'})  # `mult2.K1` mapped as `self.K12`\n",
    "        \n",
    "        # Connectors\n",
    "        self.connect(self.mult1.p_out, self.mult2.p_in)\n",
    "\n",
    "head = MultiplyComplexWithPulling(name='head')\n",
    "\n",
    "head.p_in.x = 10.\n",
    "head.K11 = 5.\n",
    "head.K12 = 5.\n",
    "head.run_once()\n",
    "\n",
    "print('head.p_in:', head.p_in)\n",
    "print('head.p_out:', head.p_out)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Connection between Systems With system full view](images/systems_5.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Classical mistakes\n",
    "\n",
    "Will you find the mistakes in the following systems? \n",
    "\n",
    "#### Exec order"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Wrong1(System):\n",
    "\n",
    "    def setup(self):\n",
    "        # Children\n",
    "        self.add_child(Multiply('mult2'), pulling={'K1': 'K12', 'p_out': 'p_out'})\n",
    "        self.add_child(Multiply('mult1'), pulling={'K1': 'K11', 'p_in': 'p_in'})\n",
    "        \n",
    "        # connectors\n",
    "        self.connect(self.mult2.p_in, self.mult1.p_out)\n",
    "\n",
    "head = Wrong1('head')\n",
    "\n",
    "head.p_in.x = 10.\n",
    "head.K11 = 5.\n",
    "head.K12 = 5.\n",
    "head.run_once()\n",
    "\n",
    "print(f\"head.p_in.x = {head.p_in.x}\")\n",
    "print(f\"head.p_out.x = {head.p_out.x}, whereas we expected {5 * 5 * 10}!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, the error is caused by a wrong execution order of the sub-systems. By default, sub-systems are computed in their declaration order; in the example above, it means `head.mult2` is computed before `head.mult1`.\n",
    "\n",
    "The execution order can be displayed with attribute `exec_order`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list(head.exec_order)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "According to the connection declared in system `head`, input `head.mult2.p_in` is mapped to output `head.mult1.p_out`, which has not been updated yet at the time sub-system `mult2` is computed.\n",
    "\n",
    "Since system `head` has been executed with `run_once`, cyclic dependencies are not resolved. Hence the difference between actual and expected results!\n",
    "\n",
    "In conclusion, the order of execution of children is important.\n",
    "When the system can be resolved without cyclic dependencies, you should make sure that the execution order is consistent with the natural flow of information.\n",
    "In class `Wrong1` above, this can be achieved by specifying\n",
    "```python\n",
    "    self.exec_order = ['mult1', 'mult2']\n",
    "```\n",
    "at the end of method `setup`.\n",
    "\n",
    "When complex coupling does involve cyclic dependencies (as is usually the case), a sensible execution order can reduce the number of unknowns necessary to equilibrate the system. Note that in this case, the system can only be balanced through iterative resolution, which in CoSApp is achieved using a `Driver`. Drivers are presented in a [dedicated tutorial](03-Drivers.ipynb).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Shadowed variables\n",
    "\n",
    "As was shown earlier, pulling a port/variable creates an attribute at parent level, automatically connected to the child attribute.\n",
    "\n",
    "However, the connector direction, that is the direction of information flow between the two connected entities, depends on the direction of the pulled port:\n",
    "\n",
    "- If the pulled port/variable is an *input*, then the connector will transfer data **downwards**, *i.e.* it will copy the parent value down to the connected child value.\n",
    "- If the pulled port/variable is an *output*, the flow is reversed, *i.e.* the connector will transfer the child value up to the parent.\n",
    "\n",
    "This has a strong implication on **pulled input variables**, which should always be **specified at parent level**.\n",
    "If you set the child input directly, its value will be superseded by the parent value before the child system is computed, and the system will not behave as expected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head = MultiplyComplexWithPulling('head')  # port `p_in` and inward `K1` pulled from `mult1`\n",
    "\n",
    "# Set pulled inputs at parent level\n",
    "head.K11 = 2.\n",
    "head.p_in.x = 10.\n",
    "# Set pulled inputs at child level\n",
    "head.mult1.K1 = 5.\n",
    "head.mult1.p_in.x = 0.\n",
    "\n",
    "head.mult2.K1 = 5.  # not pulled - OK\n",
    "\n",
    "# Execute system once\n",
    "head.run_once()\n",
    "\n",
    "print(f\"head.p_in: \\t{head.p_in}\")\n",
    "print(f\"head.p_out:\\t{head.p_out}\")\n",
    "print()\n",
    "print(f\"head.mult1.K1 = {head.mult1.K1}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Indeed, `head.mult1.K1` and `head.mult1.p_in.x` have been superseded by `head.K11` and `head.p_in.x`, respectively."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next you will learn more about [Ports](02-Ports.ipynb), the interface between systems."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
