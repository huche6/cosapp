{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tips & Tricks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Purge all drivers from a system\n",
    "\n",
    "Often necessary to start off a new, clean simulation workflow.\n",
    "\n",
    "Drivers are stored in a plain dictionary `drivers`.\n",
    "Thus, cleaning all drivers from system `s` is as simple as\n",
    "```python\n",
    "s.drivers.clear()\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save and restore system state\n",
    "\n",
    "You may quickly save and restore the state of a system in an execution sequence using functions `get_state` and `set_state` from module `cosapp.utils`:\n",
    "\n",
    "```python\n",
    "from cosapp.utils import get_state, set_state\n",
    "\n",
    "s = SomeSystem('s')\n",
    "# ... many design steps, say\n",
    "\n",
    "# Save state in local object\n",
    "designed_state = get_state(s)\n",
    "\n",
    "s.drivers.clear()\n",
    "s.add_driver(SomeDriver('driver'))\n",
    "\n",
    "try:\n",
    "    s.run_drivers()\n",
    "except:\n",
    "    # Recover previous state\n",
    "    set_state(s, designed_state)\n",
    "```\n",
    "\n",
    "Note that the objet returned by `get_state` contains the inner data of the system (both inputs *and* outputs), but does not store the description of the system (class names, sub-systems, connections, *etc.*). Data are transfered by deep (recursive) copy, to avoid all side effects between the original system and the extracted state.\n",
    "\n",
    "Consistently, `set_state` does not perform any structure check, and resets system state by deep copy operations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set targets on output values\n",
    "\n",
    "See [tutorial](Targets.ipynb) on method `add_target`."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display the internal off-design problem of a system\n",
    "\n",
    "The overall off-design problem of a system, gathered from all unknowns and equations defined at setup throughout the sub-system tree, can be extracted from method `System.assembled_problem`. By default, additional unknowns and equations arising from cyclic loop analysis (if any) are not accounted for in the assembled problem.\n",
    "To include them, one must call method `open_loops()` before problem assembly.\n",
    "\n",
    "In the next cell, we show a slight variation of the [Sellar case](optimization/Sellar.ipynb), with off-design constraints on parameters `y1`, `y2` and `z`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "from cosapp.drivers import NonLinearSolver\n",
    "import numpy as np\n",
    "\n",
    "\n",
    "class SellarDiscipline1(System):\n",
    "    \"\"\"y1 = F1(x, y2, z)\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_inward('z', np.zeros(2))\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_inward('y2', 1.0)\n",
    "\n",
    "        self.add_outward('y1', 0.0)\n",
    "\n",
    "    def compute(self):\n",
    "        self.y1 = self.z[0]**2 + self.z[1] + self.x - 0.2 * self.y2\n",
    "\n",
    "\n",
    "class SellarDiscipline2(System):\n",
    "    \"\"\"y2 = F2(y1, z)\"\"\"\n",
    "    def setup(self):\n",
    "        self.add_inward('z', np.zeros(2))\n",
    "        self.add_inward('y1', 1.0)\n",
    "\n",
    "        self.add_outward('y2', 0.0)\n",
    "\n",
    "    def compute(self):\n",
    "        self.y2 = np.sqrt(abs(self.y1)) + self.z[0] + self.z[1]\n",
    "\n",
    "\n",
    "class ConstrainedSellar(System):\n",
    "    \"\"\"System coupling disciplines 1 & 2, with off-design constraints.\n",
    "    \"\"\"\n",
    "    def setup(self):\n",
    "        d1 = self.add_child(SellarDiscipline1('d1'), pulling=['x', 'z', 'y1'])\n",
    "        d2 = self.add_child(SellarDiscipline2('d2'), pulling=['z', 'y2'])\n",
    "\n",
    "        # Couple sub-systems d1 and d2:\n",
    "        self.connect(d1, d2, ['y1', 'y2'])\n",
    "\n",
    "        # Add off-design constraints:\n",
    "        self.add_unknown('z').add_equation(['y1 == 1', 'y2 == 0.5'])\n",
    "\n",
    "\n",
    "def show_problem(problem, header=None):\n",
    "    if header:\n",
    "        print(f\"*** {header.upper()} ***\")\n",
    "    print(f\"shape = {problem.shape}\", problem, \"\", sep=\"\\n\")\n",
    "\n",
    "# Create and initialize system\n",
    "s = ConstrainedSellar('s')\n",
    "s.x = 0.1\n",
    "s.z = np.zeros(2)\n",
    "s.run_once()\n",
    "\n",
    "show_problem(s.assembled_problem(), \"Off-design problem\")\n",
    "\n",
    "# Open loops first\n",
    "s.open_loops()\n",
    "show_problem(s.assembled_problem(), \"Including loop constraints\")\n",
    "# Restore all connectors\n",
    "s.close_loops()\n",
    "\n",
    "# Solve problem (solver will eventually open loops)\n",
    "solver = s.add_driver(NonLinearSolver('solver'))\n",
    "s.run_drivers()\n",
    "show_problem(solver.problem, \"After resolution\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After loop analysis, it appears an additional unknown, `d1.y2`, must be computed in order to satisfy condition `d1.y2 == d2.y2` imposed by cyclic connections `d1.y1 -> d2.y1` and `d2.y2 -> d1.y2`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loop through all elements in a composite system tree\n",
    "\n",
    "Browsing through composite trees is usually tackled with recursive functions.\n",
    "However, writing such functions can sometimes be tricky.\n",
    "\n",
    "As of version 0.11.7, a recursive iterator makes it very easy to loop through system or driver trees.\n",
    "```python\n",
    "system = SomeComplexSystem('system')\n",
    "\n",
    "for elem in system.tree():\n",
    "    # do whatever\n",
    "```\n",
    "This functionality might come handy if one wishes to collect data from all sub-systems, say."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "\n",
    "a = System('a')\n",
    "# First layer\n",
    "a.add_child(System('aa'))\n",
    "a.add_child(System('ab'))\n",
    "# Second layer\n",
    "a.ab.add_child(System('aba'))\n",
    "a.ab.add_child(System('abb'))\n",
    "a.ab.add_child(System('abc'))\n",
    "\n",
    "print([elem.name for elem in a.tree()])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`System.tree()` accepts optional argument `downwards`, which determines whether the iterator should yield elements from top to bottom (`downwards=True`), or from bottom to top (`downwards=False` - default behaviour)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print([elem.name for elem in a.tree(downwards=True)])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the iterator follows the execution order of all elements in the tree:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print([elem.name for elem in a.tree()])\n",
    "\n",
    "a.exec_order = ['ab', 'aa']\n",
    "a.ab.exec_order = ['abc', 'aba', 'abb']\n",
    "print([elem.name for elem in a.tree()])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Synchronize sub-system inputs\n",
    "\n",
    "Inputs/inwards of a system specify the necessary information for local execution of the system.\n",
    "Very often, though, inputs of two or more sub-systems must take the same value, in the context of their parent assembly.\n",
    "\n",
    "For instance, consider a system `PlaneWing` with inward `length`.\n",
    "When gathered in a higher-level system `Airplane` as `left_wing` and `right_wing`, it may be desirable to force both wings to have the same length, by construction.\n",
    "\n",
    "Such input synchronization is natively handled by CoSApp through the pulling mechanism:\n",
    "```python\n",
    "class Airplane(System):\n",
    "    def setup(self):\n",
    "        self.add_child(PlaneWing('left_wing', pulling={'length': 'wing_length'}))\n",
    "        self.add_child(PlaneWing('right_wing', pulling={'length': 'wing_length'}))\n",
    "```\n",
    "\n",
    "### Explaination:\n",
    "Pulling an input/inward creates a parent-to-child *downward* connector.\n",
    "Thus, the first `add_child` statement will create new inward `wing_length` on the fly, as well as a connector `wing_length -> left_wing.length`.\n",
    "When the right wing sub-system is created, a second connector `wing_length -> right_wing.length` is generated, using existing inward `wing_length`.\n",
    "```python\n",
    "plane = Airplane('plane')\n",
    "\n",
    "plane.wing_length = 12.3\n",
    "plane.run_once()  # will propagate `wing_length` to both wings\n",
    "```\n",
    "### Notes\n",
    "* Connectors only transfer information when `run_once()` or `run_drivers()` is invoked.\n",
    "* Any value directly assigned to `plane.left_wing.length`, say, will be eventually superseded by `plane.wing_length` at each model execution.\n",
    "* Renaming pulled inputs/inwards at parent level is not mandatory. Passing a list of attribute names rather than a dictionary as `pulling` argument will trigger the same mechanisms, with no name changes. In the example above, it is just more explicit to expose inward `wing_length` rather than just `length` in the context of the airplane."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create collections of sub-systems and/or ports in systems\n",
    "\n",
    "Consider an arbitrary number of resistors in series, modeled as an assembly of subsystems `R1`, `R2`, *etc.*\n",
    "\n",
    "In this case, the number of resistors `n` is provided at `setup`, and the `n` resistors are created with a loop of the kind:\n",
    "```python\n",
    "    def setup(self, n=2):\n",
    "        for i in range(n)\n",
    "            self.add_child(Resistor(f\"R{i}\"))\n",
    "```\n",
    "While this is creation loop is unavoidable, it can be interesting to get an attribute storing all resistors as an iterable collection.\n",
    "\n",
    "One can pull this trick using method `add_property`, as illustrated below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import Port, System\n",
    "\n",
    "class ElecPort(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable(\"I\", 1.0, unit=\"A\", desc=\"Current\")\n",
    "        self.add_variable(\"V\", 0.0, unit=\"V\", desc=\"Voltage\")\n",
    "\n",
    "class Resistor(System):\n",
    "    def setup(self):\n",
    "        self.add_input(ElecPort, 'elec_in')\n",
    "        self.add_output(ElecPort, 'elec_out')\n",
    "        self.add_inward(\"R\", 1e2, unit=\"ohm\", desc=\"Resistance\")\n",
    "    \n",
    "    def compute(self):\n",
    "        self.elec_out.I = I = self.elec_in.I\n",
    "        self.elec_out.V = self.elec_in.V - self.R * I\n",
    "\n",
    "class ResistorSeries(System):\n",
    "    def setup(self, n=2):\n",
    "        self.add_property('n', max(int(n), 2))\n",
    "\n",
    "        # Create a tuple of children, and store it as a property\n",
    "        self.add_property('resistors', tuple(\n",
    "            self.add_child(Resistor(f\"R{i}\"))\n",
    "            for i in range(self.n)\n",
    "        ))\n",
    "        R = self.resistors\n",
    "        \n",
    "        for previous, current in zip(R, R[1:]):\n",
    "            self.connect(current.elec_in, previous.elec_out)\n",
    "\n",
    "        # Pull first `elec_in` and last `elec_out`\n",
    "        self.add_input(ElecPort, 'elec_in')\n",
    "        self.add_output(ElecPort, 'elec_out')\n",
    "\n",
    "        self.connect(self.elec_in, R[0].elec_in)\n",
    "        self.connect(self.elec_out, R[-1].elec_out)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same trick can be used to create collections of ports, if needed.\n",
    "\n",
    "Use of property `resistors` makes the code clearer, and allows one to loop through the child/port collection without resorting to syntaxes of the kind `s[f\"R{i}\"] for i in range(s.n)`.\n",
    "\n",
    "In the next cell, for example, we show two identical ways of initializing the resistances:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = ResistorSeries('s', n=5)\n",
    "\n",
    "for res in s.resistors:\n",
    "    res.R = 1.25e3\n",
    "\n",
    "# instead of:\n",
    "for i in range(s.n):\n",
    "    s[f\"R{i}\"].R = 1.25e3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note in particular that the first expression, apart from being much simpler, is also more robust to name changes in `ResistorSeries`, as we do not need to know the naming convention of sub-systems (`R1`, `R2`, *etc.*), and of their total number `n`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "System properties are immutable by nature, so `resistors` cannot be redefined. Furthermore, use of `tuple` garanties that each individual element of the collection is also immutable, which is exactly what we wish to achieve here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "\n",
    "# Check that resistors cannot be individually reassigned:\n",
    "try:\n",
    "    s.resistors[0] = None\n",
    "\n",
    "except Exception as error:\n",
    "    logging.exception(error)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Write alternative ways to create a system\n",
    "\n",
    "Consider a system class containing a 2D numpy array, which we would like to define from local data or from a file.\n",
    "In the next cell, we show a first, awkward implementation, where all parameters are provided to the `setup` method, and different courses of action are decided, depending on data type.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "import numpy\n",
    "\n",
    "class MySystem(System):\n",
    "    def setup(self, n: int, **options):\n",
    "        self.add_property('n', int(n))\n",
    "\n",
    "        self.add_inward('x', numpy.zeros((self.n, 3)))\n",
    "\n",
    "        # Get data from array\n",
    "        try:\n",
    "            x = options['x']\n",
    "        except KeyError:\n",
    "            pass\n",
    "        else:\n",
    "            if numpy.shape(x) == self.x.shape:\n",
    "                self.x = numpy.copy(x)\n",
    "        # Get data from file\n",
    "        try:\n",
    "            filename = options['filename']\n",
    "        except KeyError:\n",
    "            pass\n",
    "        else:\n",
    "            x = numpy.load(filename)\n",
    "            if x.shape == self.x.shape:\n",
    "                self.x = x\n",
    "        # And so on..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = MySystem('s', n=2, x=[[0, 0.1, 0.2], [-0.5, 0.9, 0.4]])\n",
    "\n",
    "print(s.x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The complexity of parsing through options at `setup` can be simplified using ad-hoc functions referred to as *factories*, declared as class methods by decorator `@classmethod`.\n",
    "Note that the first argument of class methods is always `cls` (as opposed to `self` for traditional object methods), denoting the class itself.\n",
    "Factories are special class method whose job is to create and return a new class instance (an object) from a specific set of arguments.\n",
    "\n",
    "Class methods are invoked with syntax `ClassName.method_name(...)`.\n",
    "\n",
    "In the next cell, we implement factory methods `from_data` and `from_file`, each with its own signature:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "import numpy\n",
    "\n",
    "class MySystem(System):\n",
    "    def setup(self, n: int):\n",
    "        self.add_property('n', int(n))\n",
    "        self.add_inward('x', numpy.zeros((self.n, 3)))\n",
    "\n",
    "    @classmethod\n",
    "    def from_data(cls, name: str, data) -> \"MySystem\":\n",
    "        n = cls.check_shape(data)\n",
    "        s = cls(name, n=n)  # newly created system\n",
    "        s.x = numpy.array(data, dtype=float)\n",
    "        return s\n",
    "\n",
    "    @classmethod\n",
    "    def from_file(cls, name: str, filename) -> \"MySystem\":\n",
    "        data = numpy.load(filename)\n",
    "        return cls.from_data(name, data)\n",
    "\n",
    "    @staticmethod\n",
    "    def check_shape(data) -> int:\n",
    "        \"\"\"Checks that `data` is a (N x 3) array-like object.\n",
    "        If so, returns integer N. If not, raises `ValueError`.\n",
    "        \"\"\"\n",
    "        shape = numpy.shape(data)\n",
    "        ok = len(shape) == 2 and shape[1] == 3\n",
    "        if not ok:\n",
    "            raise ValueError(\"data must be a (N x 3) array-like object\")\n",
    "        return shape[0]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s1 = MySystem('s1', n=4)  # usual way\n",
    "print(f\"s1.x =\\n{s1.x}\")\n",
    "\n",
    "# Create a system from existing data with `from_data`\n",
    "s2 = MySystem.from_data('s2', [[0, 0.1, 0.2], [-0.5, 0.9, 0.4]])\n",
    "print(f\"\\ns2.x =\\n{s2.x}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "\n",
    "try:\n",
    "    s3 = MySystem.from_file('s3', 'data.pickle')\n",
    "\n",
    "except Exception as error:\n",
    "    logging.error(\n",
    "        f\" {type(error).__name__}: {error}\"\n",
    "        f\"\\nDoes not work for s3 since the file does not exist, but you get the idea!\"\n",
    "    )\n",
    "else:\n",
    "    print(f\"\\ns3.x =\\n{s3.x}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
