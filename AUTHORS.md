# Credits

## Development Lead

* [Étienne Lac](https://gitlab.com/etienne.lac) (etienne.lac@safrangroup.com)

## Contributors

* [Frédéric Collonval](https://gitlab.com/fcollonval)
* [Adrien Delsalle](https://gitlab.com/adriendelsalle)
* [Guy De Spiegeleer](https://gitlab.com/GuyDS)
* [Duc Trung Lê](https://gitlab.com/ductrungle)
* Alexis Cassier (alexis.cassier@safrangroup.com)
* Amath Waly Ndiaye
* Thomas Federici (thomas.federici@safrangroup.com)
* [Jeffrey Borlik](https://gitlab.com/JeffreyBorlik) (jeffrey.borlik@safrangroup.com)
* [Adrien Talatizi](https://gitlab.com/AdrienTalatizi)
* [Mathias Malandain](https://gitlab.com/MathiasMalandain) (mathias.malandain@inria.fr)
* [Benoît Caillaud](https://gitlab.com/benoitcaillaud) (benoit.caillaud@inria.fr)
* [Haris Musaefendic](https://gitlab.com/musaefendic)
* [Román Lapuente](https://gitlab.com/roman.lapuente)
* [Peter Cairns](https://gitlab.com/petercairns_akka)
* [Virginie Zell](https://gitlab.com/VirginieZ)
